"""Initlaize database

Revision ID: init
Revises: None
Create Date: 2020-05-17 00:41:08.972267

"""

# revision identifiers, used by Alembic.
revision = 'init'
down_revision = None

from alembic import op
import sqlalchemy as sa
from sqlalchemy.dialects import postgresql
from sqlalchemy.sql import text
import re


def upgrade():
    conn = op.get_bind()
    query = re.sub('\s+', ' ', '''
        CREATE EXTENSION IF NOT EXISTS "uuid-ossp";

        CREATE SEQUENCE IF NOT EXISTS global_id_sequence;

        CREATE OR REPLACE FUNCTION id_generator(OUT result bigint) AS $$
        DECLARE
            our_epoch bigint := 1545907889956;
            seq_id bigint;
            now_millis bigint;
            shard_id int := 1;
        BEGIN
            SELECT nextval('global_id_sequence') % 1024 INTO seq_id;

            SELECT FLOOR(EXTRACT(EPOCH FROM clock_timestamp()) * 1000) INTO now_millis;
            result := (now_millis - our_epoch) << 23;
            result := result | (shard_id << 10);
            result := result | (seq_id);
        END;
        $$ LANGUAGE PLPGSQL;

        CREATE OR REPLACE FUNCTION current_timestamp_utc()
        RETURNS timestamp AS $time_in_utc$
        DECLARE
            time_in_utc timestamp;
        BEGIN
            select now() at time zone 'utc' into time_in_utc;
            RETURN time_in_utc;
        END;
        $time_in_utc$ LANGUAGE PLPGSQL;

        SELECT id_generator();

        create or replace function trigger_set_timestamp_modify()
        returns trigger as $$
        begin
          new.modified_at = current_timestamp_utc();
          return new;
        end;
        $$ language plpgsql;
     ''')

    conn.execute(text(query))

def downgrade():
    conn = op.get_bind()
    conn.execute('''
        DROP FUNCTION IF EXISTS trigger_set_timestamp_modify;
        DROP FUNCTION IF EXISTS current_timestamp_utc();
        DROP FUNCTION IF EXISTS id_generator();
        DROP SEQUENCE IF EXISTS global_id_sequence;
    ''')
