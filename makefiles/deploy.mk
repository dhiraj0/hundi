### DEPLOYMENT
# ██████████████████████████████████████████████████████████████████████

deploy.test.api: ## Test api using payload in $API_PAYLOAD
	echo ${API_PAYLOAD} > ./event && python-lambda-local -f http_server -l ./venv/lib/python3.8/site-packages ./run_lambda.py event && rm -rf event

deploy.build: ## Build the zip file for deployment
	./bin/bundlelambda ${LAMBDA_ZIP}

deploy: deploy.build ## Deploy lambda into AWS
	./bin/deployterraform
