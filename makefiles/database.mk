### DATABASE
# ██████████████████████████████████████████████████████████████████████


database.connect: ## Connect to database
	docker-compose -f ./docker-compose-dev.yaml exec db psql -U ${PGUSER} --db ${PGDATABASE}

database.create.migration: ## Create alembic migration file
	python src/manage.py db migrate

database.upgrade: ## Upgrade to latest migration
	python src/manage.py db upgrade

database.downgrade: ## Downgrade latest migration
	python src/manage.py db downgrade
