### LOCAL SERVER
# ██████████████████████████████████████████████████████████████████████

.ONESHELL:

local.install: ## Prepare local installation
	@echo ""
	@printf "\033[31m%-30s\033[0m %s\n" "███████╗███████╗████████╗██╗   ██╗"
	@printf "\033[32m%-30s\033[0m %s\n" "██╔════╝██╔════╝╚══██╔══╝██║   ██║"
	@printf "\033[36m%-30s\033[0m %s\n" "███████╗█████╗     ██║   ██║   ██║"
	@printf "\033[34m%-30s\033[0m %s\n" "╚════██║██╔══╝     ██║   ██║   ██║"
	@printf "\033[35m%-30s\033[0m %s\n" "███████║███████╗   ██║   ╚██████╔╝"
	@printf "\033[36m%-30s\033[0m %s\n" "╚══════╝╚══════╝   ╚═╝    ╚═════╝ "
	@if [[ ! -e "venv" ]]; then virtualenv venv && source ./venv/bin/activate && pip install -r ./requirements.txt; else echo "Already initialized"; fi

venv:
	source venv/bin/activate

local.start: venv ## Start the server on local
	@FLASK_SKIP_DOTENV=1 nodemon --watch src/ -e py --signal SIGKILL --exec 'python src/server.py'

local.watch: venv ## Start and watch the server on local
	@make start

local.services: ## Start DB and other services
	source .envrc
	@docker-compose -f ./docker-compose-dev.yaml up

local.services.build: ## Build DB and other services
	source .envrc
	@docker-compose rm db
	@docker volume rm lending_db
	@docker volume create --name=lending_db
	@docker-compose -f ./docker-compose-dev.yaml build && docker-compose -f ./docker-compose-dev.yaml up

