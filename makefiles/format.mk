### FORMAT
# ██████████████████████████████████████████████████████████████████████

COVERAGE_MIN = 70

format.isort: ## Sort imports
	isort -rc src/ test/ --skip src/models/__init__.py

.PHONY: test
test: ## Launch tests in their own docker container
	source .envrc && py.test --disable-pytest-warnings test/

.PHONY: coverage
test.coverage: ## Generate test coverage
	source .envrc && py.test --disable-pytest-warnings --cov-report term --cov-report html:coverage --cov-config setup.cfg --cov=src/ test/

test.lint: ## Lint python files with pylint
	pylint ./src ./test
