# isengard

Boilerplate Flask app for serverless APIs.

## Setup

#### 1. Create `.envrc` file and update contents
This is used to set the environment variables required for deployment and local development.
```
$ vim .envrc
```

#### 2. Create a virtualenv then install requirements:
```
$ make install
```

## Development

#### 1. Set Environment

Ensure you have created your virtualenv and have the necessary environment variables set (see [setup instructions](#setup) above).

```
$ source env/bin/activate
$ source .envrc
```
#### 2. Run Services

##### On host
```
$ make services
```

#### 3. Run server

##### On host
```
$ make start
```

## Devops

### Terraform Deployment

Ensure you have created your virtualenv and have the necessary environment variables set (see [setup instructions](#setup) above).

##### Setup
Create terraform state bucket.
```
$ aws s3 mb --region eu-west-2 s3://setu-lambda-isengard
```

#### Deploy
Bundle the app into a zip and deploy it to respective environment using terraform.
```
source ./deployment/terraform/.envrc
make deploy
```

## Test
```
$ make test
$ make test.coverage
```

# Process via CURL

1. Associate an endpoint with a user token

```bash
curl -X GET "https://3oz36r7gd0.execute-api.ap-southeast-1.amazonaws.com/default/endpoints?user_id=1" -H "accept: application/json"
```

2. Verify if the changes have been made

```bash
curl -X POST "https://3oz36r7gd0.execute-api.ap-southeast-1.amazonaws.com/default/endpoints?user_id=1" -H "accept: application/json" -H "Content-Type: application/json" -d "{ \"host\": \"sandbox.setu.co\", \"paths\": [ \"/api/code/version\" ], \"port\": 80, \"ssl\": true}"
```

3. Hit an endpoint

```bash
curl -X POST "https://3oz36r7gd0.execute-api.ap-southeast-1.amazonaws.com/default/call?id=416ba5dd-34a4-4105-9b9d-865c9814f848&user_id=1" -H "accept: application/json" -H "Content-Type: application/json" -d "{ \"arguments\": \"id=4&value=lol\", \"body\": \"{}\", \"headers\": \"{\\\"content-type\\\": \\\"application/json\\\"}\", \"type\": \"GET\", \"verify_ssl_cert\": true}"
```

