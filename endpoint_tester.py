import logging
import json
import re
from datetime import datetime
import random
import base64
import requests

from Crypto.Cipher import AES
from Crypto.Hash import SHA512

import xml.etree.ElementTree as ET
import xml.dom.minidom as minidom

from .src.log import log

EQUITAS_API_KEY = "dfqaj44bdqrwbe55npzy45mv"
CS_KEY_PLAIN = "6cZq3Y0lV1Rq71GBwXhS8A=="
CS_KEY = base64.b64decode(b'6cZq3Y0lV1Rq71GBwXhS8A==')
CRYPT_KEY = base64.b64decode(b'qVTyD/36Ts8R2qqtBYL7WQ==')
cde = "FI0029"
appId = "FOS"
brnchId = "9999"
usrId = "IBUSER"
Mcc = "6012"
PosEntryMode = "019"
PosCode = "05"
aadharNumber = "562711904686"
CA_ID = "EQT000000000001"


def geteKYCXMLStringReqPayload(now):
    return stripWhiteSpaceInXML(
        """
    <OtpRequest>
        <TransactionInfo>
                <UID type='U'>{0}</UID>
                <Transm_Date_time>{1}</Transm_Date_time>
                <Stan>{2}</Stan>
                <Local_Trans_Time>{3}</Local_Trans_Time>
                <Local_date>{4}</Local_date>
                <Mcc>{5}</Mcc>
                <Pos_entry_mode>{6}</Pos_entry_mode>
                <Pos_code>{7}</Pos_code>
                <CA_ID>{8}</CA_ID>
                <CA_TID>11205764</CA_TID>
                <CA_TA>CSB NERUL MUMBAI                    MHIN</CA_TA>
        </TransactionInfo>
        <Opts ch='01'/>
    </OtpRequest>
    """.format(
            aadharNumber,
            now.utcnow().strftime("%m%d%H%M%S"),
            random.randint(1, 5000),
            now.strftime("%H%M%S"),
            now.strftime("%m%d"),
            Mcc,
            PosEntryMode,
            PosCode,
            CA_ID
        )
    )


def parseeKYCXMLStringRepPayload(xml_string):
    # for now it just pretty prints xml
    xml = minidom.parseString(xml_string)
    log.info(xml.toprettyxml())


def generateDigiOTPRequestMessageBuilder(now, eKYCXMLStringReqPayload):
    CNV_ID = makeConversationId(now)
    return {
        "generateDigiOTPRequest": {
            "msgHdr": {
                "msgId": CNV_ID,
                "cnvId": CNV_ID,
                "extRefId": CNV_ID,
                "appId": appId,
                "timestamp": now.utcnow().isoformat(),
                "authInfo": {
                    "brnchId": brnchId,
                    "usrId": usrId
                }
            },
            "msgBdy": {
                "eKYCXMLStringReqPayload": toBase64(eKYCXMLStringReqPayload)
            }
        }
    }


def generateDigiOTPResponseParser(response):
    isOk = response["generateDigiOTPResponse"]["msgHdr"]["rslt"]
    eKYCXMLStringRepPayload = response["generateDigiOTPResponse"]["msgBdy"]["eKYCXMLStringRepPayload"]
    if isOk != "OK":
        raise ValueError("generateDigiOTPResponse is not OK")
    return base64.b64decode(eKYCXMLStringRepPayload).decode("utf-8")


def _pad(s):
    bs = int(AES.block_size)
    pad = (bs - len(s) % bs) * chr(bs - len(s) % bs)
    return s + bytes(pad.encode("utf-8"))


def _unpad(s):
    return s[:-ord(s[len(s) - 1:])]


def toBase64(string):
    return base64.b64encode(string.encode("utf-8")).decode("utf-8")


def makeXMLWithPayloadAndChecksum(ESB_REQUEST, CHECKSUM):
    return stripWhiteSpaceInXML("""<?xml version="1.0" encoding="UTF-8"?><PIDBlock>
            <payload>{0}</payload>
            <checksum>{1}</checksum>
        </PIDBlock>""".format(ESB_REQUEST, CHECKSUM))


def parseXMLWithPayloadAndChecksum(xml_string):
    xml_root = ET.fromstring(xml_string)
    payload = xml_root.find("payload").text
    checksum = xml_root.find("checksum").text
    return {"payload": payload, "checksum": checksum}


def makeCheckSum2(body):
    beforeDigest = body + CS_KEY_PLAIN
    return SHA512.new(beforeDigest.encode("utf-8")).hexdigest().upper()


def encrypt(body):
    body = json.dumps(body)
    checksum = makeCheckSum2(body)

    xml = makeXMLWithPayloadAndChecksum(body, checksum)

    cipher_aes = AES.new(CRYPT_KEY, AES.MODE_ECB)
    ciphertext = cipher_aes.encrypt(
        _pad(
            xml.encode("utf-8")
        )
    )
    return base64.b64encode(ciphertext).decode("utf-8")


def decrypt(payload):
    ciphertext = base64.b64decode(payload.encode("utf-8"))
    cipher_aes = AES.new(CRYPT_KEY, AES.MODE_ECB)
    plaintext = _unpad(
        cipher_aes.decrypt(ciphertext)
    ).decode("utf-8")
    return plaintext


def stripWhiteSpaceInXML(xml_string):
    return re.sub('\s+(?=<)', '', xml_string)


def makeConversationId(now):
    return "SET" + now.strftime("%Y%m%d%H%M%S%f") + str(random.randint(10, 99))


class EquitasService:

    def __init__(self, now):
        self.headers = {
            "api_key": EQUITAS_API_KEY,
            "content-type": "application/json",
            "bc_id": "821"
        }
        self.encrypted_request = {
            "req_root": {
                "header": {
                    "cde": cde,
                    "requestId": str(random.randint(1, 5000)),
                    "version": "1.0",
                    "dateTime": now.isoformat()
                },
                "body": {
                    "payload": "/**encrypted payload(self.body) goes here*/"
                }
            }
        }
        self.encrypted_response = None

    def request(self, url, message, helptext):

        self.encrypted_request["req_root"]["body"]["payload"] = encrypt(message)

        # -----------------------------------------------------------------------------
        # |||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
        log.info("Sending %s request to EQUITAS: %s" % (helptext, url))
        log.info(json.dumps(self.headers, indent=2) + '\n-----------\n' + json.dumps(message) + '\n-----------\n' + json.dumps(
            self.encrypted_request, indent=2))
        # |||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
        # -----------------------------------------------------------------------------
        response = requests.post(url, headers=self.headers,
                                 data=json.dumps(self.encrypted_request))
        # -----------------------------------------------------------------------------
        # |||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
        log.warn("Recieved response from EQUITAS:%s" % (response.text))
        # |||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
        # -----------------------------------------------------------------------------
        self.encrypted_response = json.loads(response.text)

        decrypted_xml = decrypt(self.encrypted_response["req_root"]["body"]["payload"])
        payloadAndChecksum = parseXMLWithPayloadAndChecksum(decrypted_xml)
        if(payloadAndChecksum["checksum"] != makeCheckSum2(payloadAndChecksum["payload"])):
            raise ValueError("Checksum of response is invalid")
        return payloadAndChecksum["payload"]


if __name__ == "__main__":
    url = "https://apigwuat.equitasbank.com/uat/ext/digibnk/onboarding/generate/ekyc/otp/secure/v1"
    now = datetime.now()

    msg_inner_xml = geteKYCXMLStringReqPayload(now)
    # ///// we now have the inner XML consumed by Sarvatra ///////////////
    msg = generateDigiOTPRequestMessageBuilder(now, msg_inner_xml)
    # ////// we have the message now //////////////////////////////////////
    srv = EquitasService(now)
    response = srv.request(url, msg, "FETCH OTP FOR EKYC PROCESS")
    response = json.loads(response)
    # ///// we have the reply now ////////////////////////////////////////
    response_inner_xml = generateDigiOTPResponseParser(response)
    # //// we now have the inner XML returned by Sarvatra ////////////////
    parseeKYCXMLStringRepPayload(response_inner_xml)
