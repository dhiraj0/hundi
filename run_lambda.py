#!/usr/bin/env python3

import sys, os
sys.path.insert(0,'.')
sys.path.insert(1,'./src')

d = '.'

# [sys.path.insert(i+2, o) for i, o in enumerate(os.listdir(d))
#                     if os.path.isdir(os.path.join(d,o))]

from src import run

http_server = run()

print('█████████████████████████████████████████████████████████████████████████')
print('████████████████████████████Exiting server ██████████████████████████████')
print('█████████████████████████████████████████████████████████████████████████')
