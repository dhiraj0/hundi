resource "aws_db_instance" "setu_db" {
  allocated_storage               = 20 # gigabytes
  allow_major_version_upgrade     = false
  apply_immediately               = false
  auto_minor_version_upgrade      = false
  backup_retention_period         = 30 # in days
  db_subnet_group_name            = aws_db_subnet_group.setu_db.name
  engine                          = "postgres"
  engine_version                  = "11.4"
  identifier                      = "${var.project_name}-db"
  instance_class                  = var.db_instance_type
  maintenance_window              = "Tue:22:00-Tue:23:59" # This is in UTC - Midnight before Wednesday begins
  monitoring_interval             = "0"                   # Enhanced monitoring disabled for now
  multi_az                        = true
  name                            = "dolphin"                            # Default DB to create when the RDS env is spawned
  password                        = random_string.rds_db_password.result # Use a randomly generated password
  port                            = 5432
  publicly_accessible             = false
  storage_encrypted               = true # you should always do this
  storage_type                    = "gp2"
  username                        = "setudbuser"
  vpc_security_group_ids          = [aws_security_group.setu_db.id]
  parameter_group_name            = aws_db_parameter_group.setu_db.name
  enabled_cloudwatch_logs_exports = ["postgresql", "upgrade"]

  timeouts {
    create = "90m"
    delete = "2h"
  }

  tags = {
    "Project" = var.project_name
  }
}

resource "aws_db_subnet_group" "setu_db" {
  name       = "${var.project_name}-rds-subnet-group"
  subnet_ids = module.lambda_api_gateway.private_subnet_ids
  tags = {
    "Project" = var.project_name
  }
}

resource "aws_security_group" "setu_db" {
  name        = "${var.project_name}_setu_db"
  description = "RDS postgres server for setu"
  vpc_id      = module.lambda_api_gateway.vpc_id

  tags = {
    "Project" = var.project_name
  }
}

resource "aws_security_group_rule" "allow-vpc-postgres" {
  description       = "Allow machines in this VPC to connect to postgres"
  from_port         = 5432
  protocol          = "tcp"
  to_port           = 5432
  type              = "ingress"
  security_group_id = aws_security_group.setu_db.id
  cidr_blocks       = concat(var.public_subnets_cidr, var.private_subnets_cidr, var.nat_cidr)
}

resource "aws_db_parameter_group" "setu_db" {
  family = "postgres11"

  parameter {
    name  = "log_connections"
    value = "1"
  }

  parameter {
    name  = "log_disconnections"
    value = "1"
  }

  parameter {
    name  = "log_duration"
    value = "1"
  }

  parameter {
    name  = "log_checkpoints"
    value = "0"
  }

  parameter {
    name  = "default_transaction_isolation"
    value = "serializable"
  }
}

resource "random_string" "rds_db_password" {
  length  = 32
  special = false
}

#########################################################
# RDS alarms
#########################################################

resource "aws_cloudwatch_metric_alarm" "alarm_db_cpu" {
  alarm_name          = "${var.project_name}-alarm_db_cpu"
  comparison_operator = "GreaterThanThreshold"
  evaluation_periods  = "1"
  metric_name         = "CPUUtilization"
  namespace           = "AWS/RDS"
  period              = "60"
  statistic           = "Maximum"
  threshold           = var.max_db_cpu_usage
  dimensions = {
    DBInstanceIdentifier = "${var.project_name}-db"
  }
  alarm_description = "Voice call if DB cpu usage increases beyond threshold"
  alarm_actions     = [aws_sns_topic.voice_alerts.arn]

  tags = {
    "Project" = var.project_name
  }
}

resource "aws_cloudwatch_metric_alarm" "alarm_db_load" {
  alarm_name          = "${var.project_name}-alarm_db_load"
  comparison_operator = "GreaterThanThreshold"
  evaluation_periods  = "1"
  metric_name         = "DBLoad"
  namespace           = "AWS/RDS"
  period              = "60"
  statistic           = "Maximum"
  threshold           = var.max_db_load
  dimensions = {
    DBInstanceIdentifier = "${var.project_name}-db"
  }
  alarm_description = "Voice call if DB load increases beyond threshold"
  alarm_actions     = [aws_sns_topic.voice_alerts.arn]

  tags = {
    "Project" = var.project_name
  }
}

resource "aws_cloudwatch_metric_alarm" "alarm_db_connections" {
  alarm_name          = "${var.project_name}-alarm_db_connections"
  comparison_operator = "GreaterThanThreshold"
  evaluation_periods  = "1"
  metric_name         = "DatabaseConnections"
  namespace           = "AWS/RDS"
  period              = "60"
  statistic           = "Maximum"
  threshold           = var.max_db_connections
  dimensions = {
    DBInstanceIdentifier = "${var.project_name}-db"
  }
  alarm_description = "SMS if DB connections increases beyond threshold"
  alarm_actions     = [aws_sns_topic.sms_alerts.arn]

  tags = {
    "Project" = var.project_name
  }
}

resource "aws_cloudwatch_metric_alarm" "alarm_db_storage_space" {
  alarm_name          = "${var.project_name}-alarm_db_storage_space"
  comparison_operator = "LessThanThreshold"
  evaluation_periods  = "1"
  metric_name         = "FreeStorageSpace"
  namespace           = "AWS/RDS"
  period              = "60"
  statistic           = "Minimum"
  threshold           = var.min_db_storage_bytes
  dimensions = {
    DBInstanceIdentifier = "${var.project_name}-db"
  }
  alarm_description = "Voice call if DB storage space decreases below threshold"
  alarm_actions     = [aws_sns_topic.voice_alerts.arn]

  tags = {
    "Project" = var.project_name
  }
}

output "rds_password" {
  value = random_string.rds_db_password.result
}
