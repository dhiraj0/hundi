output "api_url" {
  value = module.api.api_url
}

output "lambda_zip" {
  value = "${aws_s3_bucket.lambda_repo.bucket}/${var.lambda_zip_path}"
}

output "vpc_id" {
  value = module.vpc_subnets.vpc_id
}

output "public_subnet_ids" {
  value = module.vpc_subnets.public_subnet_ids
}

output "private_subnet_ids" {
  value = module.vpc_subnets.private_subnet_ids
}

output "cidr" {
  value = module.vpc_subnets.cidr
}

output "nat_subnet_id" {
  value = module.vpc_subnets.nat_subnet_id
}

output "nat_subnet_cidr" {
  value = module.vpc_subnets.nat_subnet_cidr
}

