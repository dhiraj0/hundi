#!/usr/bin/env bash

echo "===================================================="
echo "This script will re-generate AWS lambda function packages"
echo ""
echo ""
echo "===================================================="

if [ ! -x "$(command -v npm)" ]; then
	echo "Please install npm before proceeding"
	exit 1
fi

if [ ! -x "$(command -v serverless)" ]; then
	echo "Installing serverless"
	sudo npm -i -g serverless
fi

[[ -z "$project_name" ]] && { echo "project_name is empty" ; exit 1; }

echo "===================================================="
echo "Building rocketchat λ"
echo "===================================================="
cd ../serverless/rocketchat_webhook
cat serverless.yml.template | \
    awk '{gsub("PROJECT_NAME", ENVIRON["project_name"], $0); print}' \
    > serverless.yml
serverless package --package rocketchat_webhook
mv rocketchat_webhook/rocketchat-webhook.zip ../../bbps/terraform

echo "===================================================="
echo "Building SMS λ"
echo "===================================================="
cd ../sms_webhook
cat serverless.yml.template | \
    awk '{gsub("PROJECT_NAME", ENVIRON["project_name"], $0); print}' \
    > serverless.yml
serverless package --package sms_webhook
mv sms_webhook/sms-webhook.zip ../../bbps/terraform

echo "===================================================="
echo "Building voice λ"
echo "===================================================="
cd ../voice_webhook
cat serverless.yml.template | \
    awk '{gsub("PROJECT_NAME", ENVIRON["project_name"], $0); print}' \
    > serverless.yml
serverless package --package voice_webhook
mv voice_webhook/voice-webhook.zip ../../bbps/terraform

cd ../terraform
