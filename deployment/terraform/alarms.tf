#########################################################
# IAM role for triggering lambda functions
#########################################################

resource "aws_iam_role" "cloudwatch_alarms_lambda" {
  name = "${var.project_name}-cloudwatch_alarms_lambda"

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "lambda.amazonaws.com"
      },
      "Effect": "Allow",
      "Sid": ""
    }
  ]
}
EOF

}

resource "aws_iam_policy" "lambda_logging" {
  name        = "${var.project_name}-lambda_logging"
  path        = "/"
  description = "IAM policy for logging from a lambda"

  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": [
        "logs:CreateLogGroup",
        "logs:CreateLogStream",
        "logs:PutLogEvents"
      ],
      "Resource": "arn:aws:logs:*:*:*",
      "Effect": "Allow"
    }
  ]
}
EOF

}

resource "aws_iam_role_policy_attachment" "lambda_logs" {
  role       = aws_iam_role.cloudwatch_alarms_lambda.name
  policy_arn = aws_iam_policy.lambda_logging.arn
}

#########################################################
# SNS topics, each connected to lambda functions
# triggering appropriate actions
#########################################################

resource "aws_sns_topic" "sms_alerts" {
  name = "${var.project_name}-sms-alerts"
}

resource "aws_sns_topic" "voice_alerts" {
  name = "${var.project_name}-voice-alerts"
}

resource "aws_sns_topic" "rocketchat_alerts" {
  name = "${var.project_name}-rocketchat-alerts"
}

#########################################################
# Lambda for voice alerts
#########################################################

resource "aws_sns_topic_subscription" "voice_alerts" {
  topic_arn = aws_sns_topic.voice_alerts.arn
  protocol  = "lambda"
  endpoint  = aws_lambda_function.voice_alerts.arn
}

resource "aws_lambda_function" "voice_alerts" {
  filename         = "voice-webhook.zip"
  function_name    = "${var.project_name}-voice_alerts"
  role             = aws_iam_role.cloudwatch_alarms_lambda.arn
  handler          = "handler.hello"
  runtime          = "python3.8"
  source_code_hash = filebase64sha256("voice-webhook.zip")

  environment {
    variables = {
      project     = var.project_name
      webhook_url = var.voice_webhook_url
    }
  }
}

resource "aws_lambda_permission" "voice_alerts" {
  statement_id  = "AllowExecutionFromSNS"
  action        = "lambda:InvokeFunction"
  function_name = aws_lambda_function.voice_alerts.arn
  principal     = "sns.amazonaws.com"
  source_arn    = aws_sns_topic.voice_alerts.arn
}

#########################################################
# Lambda for SMS alerts
#########################################################

resource "aws_sns_topic_subscription" "sms_alerts" {
  topic_arn = aws_sns_topic.sms_alerts.arn
  protocol  = "lambda"
  endpoint  = aws_lambda_function.sms_alerts.arn
}

resource "aws_lambda_function" "sms_alerts" {
  filename         = "sms-webhook.zip"
  function_name    = "${var.project_name}-sms_alerts"
  role             = aws_iam_role.cloudwatch_alarms_lambda.arn
  handler          = "handler.hello"
  runtime          = "python3.8"
  source_code_hash = filebase64sha256("sms-webhook.zip")

  environment {
    variables = {
      project     = var.project_name
      webhook_url = var.sms_webhook_url
    }
  }
}

resource "aws_lambda_permission" "sms_alerts" {
  statement_id  = "AllowExecutionFromSNS"
  action        = "lambda:InvokeFunction"
  function_name = aws_lambda_function.sms_alerts.arn
  principal     = "sns.amazonaws.com"
  source_arn    = aws_sns_topic.sms_alerts.arn
}

#########################################################
# Lambda for rocketchat alerts
#########################################################

resource "aws_sns_topic_subscription" "rocketchat_alerts" {
  topic_arn = aws_sns_topic.rocketchat_alerts.arn
  protocol  = "lambda"
  endpoint  = aws_lambda_function.rocketchat_alerts.arn
}

resource "aws_lambda_function" "rocketchat_alerts" {
  filename         = "rocketchat-webhook.zip"
  function_name    = "${var.project_name}-rocketchat_alerts"
  role             = aws_iam_role.cloudwatch_alarms_lambda.arn
  handler          = "handler.hello"
  runtime          = "python3.8"
  source_code_hash = filebase64sha256("rocketchat-webhook.zip")

  environment {
    variables = {
      project     = var.project_name
      webhook_url = var.rocketchat_webhook_url
    }
  }
}

resource "aws_lambda_permission" "rocketchat_alerts" {
  statement_id  = "AllowExecutionFromSNS"
  action        = "lambda:InvokeFunction"
  function_name = aws_lambda_function.rocketchat_alerts.arn
  principal     = "sns.amazonaws.com"
  source_arn    = aws_sns_topic.rocketchat_alerts.arn
}

