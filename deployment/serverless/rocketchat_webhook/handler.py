import json
import urllib.parse
import urllib.request

ROCKETCHAT_URL = 'https://chat.setu.co/hooks/GKNKvdY7Fky4maeWC/W63zuzzx4ZsCzHDWuXae5nMJ8WvJEDaySLHyz85iy2LAMJrj'

BASE_MSG = {"icon_emoji":":scream_cat:","text":"","attachments":[]}


def hello(event, context):

    alerts = []
    print((event))

    for record in event['Records']:
        subject = record['Sns']['Subject']
        message = json.dumps(record['Sns']['Message'], indent=4)
        alerts.append(
            {
                "message": message,
                "input": event,
                "subject": subject
            }
        )

        # Send a rocketchat message
        BASE_MSG['text'] = '# AWS Cloudwatch Alarm' + '\n\n## ' + subject + '\n\n' +  '```' + message + '```' + '\n\n'
        data = urllib.parse.urlencode(BASE_MSG)
        data = data.encode('ascii')
        req = urllib.request.Request(ROCKETCHAT_URL)
        with urllib.request.urlopen(req, data) as response:
           resp = response.read()
           print('Webhook triggered successfully')

        print(subject)
        print(message)

    response = {
        "statusCode": 200,
        "body": json.dumps(alerts)
    }

    return response
