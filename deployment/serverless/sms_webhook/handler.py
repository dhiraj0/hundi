import json
import urllib.parse
import urllib.request
import base64

ROCKETCHAT_URL = 'https://chat.setu.co/hooks/GKNKvdY7Fky4maeWC/W63zuzzx4ZsCzHDWuXae5nMJ8WvJEDaySLHyz85iy2LAMJrj'
VOICE_URL = 'https://api.exotel.com/v1/Accounts/setu53/Calls/connect.json'
SMS_URL = 'https://api.exotel.com/v1/Accounts/setu53/Sms/send.json'

EXOTEL_USER='694c42603acf54271e4846be35e3168a599b231ea1f34a81'
EXOTEL_PASS='c06141fd9386b8616baff1273ce53bac7e3481fdd09a4c11'

BASE_MSG = {"icon_emoji":":scream_cat:","text":"","attachments":[]}
RECIPIENTS = ['7829666218', '9019925624', '9019925623', '9611135570']

def hello(event, context):

    alerts = []
    print((event))

    for record in event['Records']:
        subject = record['Sns']['Subject']
        message = json.dumps(record['Sns']['Message'], indent=4)
        alerts.append(
            {
                "message": message,
                "input": event,
                "subject": subject
            }
        )

        # Send an SMS to all
        for r in RECIPIENTS:
            try:
                data = {
                    "From": "08047193156",
                    "CallerId": r,
                    "Body": subject
                }
                data = urllib.parse.urlencode(data)
                data = data.encode('ascii')

                password_mgr = urllib.request.HTTPPasswordMgrWithDefaultRealm()
                password_mgr.add_password(None, 'https://api.exotel.com', EXOTEL_USER, EXOTEL_PASS)
                handler = urllib.request.HTTPBasicAuthHandler(password_mgr)
                opener = urllib.request.build_opener(handler)
                urllib.request.install_opener(opener)

                req = urllib.request.Request(SMS_URL, data)
                with urllib.request.urlopen(req) as response:
                   resp = response.read()
                   print('Webhook triggered successfully')
            except Exception as e:
                pass

        # Send a rocketchat message
        BASE_MSG['text'] = '# AWS Cloudwatch Alarm' + '\n\n## ' + subject + '\n\n' +  '```' + message + '```' + '\n\n'
        data = urllib.parse.urlencode(BASE_MSG)
        data = data.encode('ascii')
        req = urllib.request.Request(ROCKETCHAT_URL)
        with urllib.request.urlopen(req, data) as response:
           resp = response.read()
           print('Webhook triggered successfully')

        print(subject)
        print(message)

    response = {
        "statusCode": 200,
        "body": json.dumps(alerts)
    }

    return response
