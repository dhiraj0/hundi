#!/usr/bin/env python3

from flask.blueprints import Blueprint
from flask_lambda import FlaskLambda
from flask_migrate import Migrate, upgrade
from flask import Flask, request
from flasgger import Swagger, LazyString, LazyJSONEncoder
from werkzeug.middleware.http_proxy import ProxyMiddleware

from log import log
import config
import routes
from models import db


class ReverseProxied(object):
    '''Wrap the application in this middleware and configure the
    front-end server to add these headers, to let you quietly bind
    this to a URL other than / and to an HTTP scheme that is
    different than what is used locally.
    '''

    def __init__(self, app):
        self.app = app

    def __call__(self, environ, start_response):
        environ['SCRIPT_NAME'] = '/default'
        return self.app(environ, start_response)


# config your API specs
# you can define multiple specs in the case your api has multiple versions
# omit configs to get the default (all views exposed in /spec url)
# rule_filter is a callable that receives "Rule" object and
#   returns a boolean to filter in only desired views

def run():
    server = FlaskLambda(__name__)

    server.config["SWAGGER"] = {
        "swagger_version": "2.0",
        "title": "Setu isengard",
        "specs": [
            {
                "version": "0.1.0",
                "le": "Setu isengard",
                "endpoint": "spec",
                "route": "/spec",
                "rule_filter": lambda rule: True,  # all in
            }
        ],
        "static_url_path": "/docs",
        "specs_route": "/docs/",
        "swagger_ui_bundle_js": "//unpkg.com/swagger-ui-dist@3/swagger-ui-bundle.js",
        "swagger_ui_standalone_preset_js": "//unpkg.com/swagger-ui-dist@3/swagger-ui-standalone-preset.js",
        "jquery_js": "//unpkg.com/jquery@2.2.4/dist/jquery.min.js",
        "swagger_ui_css": "//unpkg.com/swagger-ui-dist@3/swagger-ui.css",
    }

    template = dict(swaggerUiPrefix="/default")
    Swagger(server, template=template)

    log.warning("███████╗███████╗████████╗██╗   ██╗")
    log.warning("██╔════╝██╔════╝╚══██╔══╝██║   ██║")
    log.warning("███████╗█████╗     ██║   ██║   ██║")
    log.warning("╚════██║██╔══╝     ██║   ██║   ██║")
    log.warning("███████║███████╗   ██║   ╚██████╔╝")
    log.warning("╚══════╝╚══════╝   ╚═╝    ╚═════╝ ")

    server.debug = config.DEBUG
    server.config["SQLALCHEMY_DATABASE_URI"] = config.DB_URI
    server.config["SQLALCHEMY_TRACK_MODIFICATIONS"] = config.SQLALCHEMY_TRACK_MODIFICATIONS
    db.init_app(server)
    db.app = server

    # configure reverse-proxies
    server.wsgi_app = ReverseProxied(server.wsgi_app)
    # app = ProxyMiddleware(server, {
    #     "/default": {
    #         "target": "http://127.0.0.1/",
    #     }
    # })

    if config.MIGRATE_ON_START:
        log.warning("Running DB upgrade")
        with server.app_context():
            migrate = Migrate(server, db)
            upgrade('migrations')

    for blueprint in vars(routes).values():
        if isinstance(blueprint, Blueprint):
            server.register_blueprint(blueprint, url_prefix=config.APPLICATION_ROOT)
    return server
