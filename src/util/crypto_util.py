import base64
from Crypto.Cipher import AES


def pad(s):
    bs = int(AES.block_size)
    padding = (bs - len(s) % bs) * chr(bs - len(s) % bs)
    return s + bytes(padding.encode("utf-8"))


def unpad(s):
    return s[:-ord(s[len(s) - 1:])]


def toBase64(string):
    if(type(string) is str):
        return base64.b64encode(string.encode("utf-8")).decode("utf-8")
    return base64.b64encode(string).decode("utf-8")
