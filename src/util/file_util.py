import base64


def readFileInB64(filename):
    with open(filename, "rb") as image_file:
        file_in_b64 = base64.b64encode(image_file.read()).decode("utf-8")
        return file_in_b64
