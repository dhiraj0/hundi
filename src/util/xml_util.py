import re


def sanitizeXMLstring(xml_string):
    return re.sub('\s+(?=<)', '', xml_string).strip()
