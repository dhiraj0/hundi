"""
Define the REST verbs relative to the users
"""

from flasgger import swag_from, validate
from flask.json import jsonify
from flask_restful import Resource
from flask_restful.reqparse import Argument

from repositories import AccessRepository
from util import parse_params


class GetAccessLog(Resource):
  @staticmethod
  @parse_params(
    Argument("id", location="values", required=True, help="The id of the log entry.")
  )
  @swag_from("../swagger/access/GET.yml", validation=True)
  def get(id):
    """ Return an access log entry by id """
    entry = AccessRepository.get(by='id', id=id)
    validate({"entry": entry.json}, filepath="../swagger/access/GET.yml")
    return jsonify({"entry": entry.json})


class GetAccessLogByUser(Resource):
  @staticmethod
  @parse_params(
    Argument("user_id", location="args", required=True, help="The id of the user.")
  )
  @swag_from("../swagger/access/GET_USER.yml", validation=False)
  def get(user_id):
    """ Return an access log entry by user id """
    logs = AccessRepository.get(by='user_id', user_id=user_id)
    ret = ({
      "user": user_id,
      "logs": [ x.json for x in logs ]
    })
    validate(ret, filepath="../swagger/access/GET_USER.yml")
    return jsonify(ret)

  @staticmethod
  @parse_params(
    Argument("user_id", location="args", required=True, help="The id of the user."),
    Argument("data", location="json", required=True, help="Data related to this access.")
  )
  @swag_from("../swagger/access/POST.yml", validation=True)
  def post(user_id, data):
    """ Create an user based on the sent information """
    access = AccessRepository.create(
      destination_url='',
      user_id=user_id,
      data=data
    )
    validate({"entry": access.json}, filepath="../swagger/access/POST.yml")
    return jsonify({"log": access.json})
