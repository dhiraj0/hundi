"""
Define the REST verbs relative to the users
"""

from flasgger import swag_from, validate
from flask.json import jsonify
from flask_restful import Resource
from flask_restful.reqparse import Argument

from repositories import EndpointRepository
from util import parse_params


class EndpointManager(Resource):
  @staticmethod
  @parse_params(
    Argument("user_id", location="args", required=True, help="The id of the user.")
  )
  @swag_from("../swagger/endpoint/GET.yml", validation=False)
  def get(user_id):
    """ Return an endpoint entry by id """
    entries = EndpointRepository.get(by='user_token', user_token=user_id)
    ret = {"endpoints": [ x.json for x in entries ]}
    validate(ret, filepath="../swagger/endpoint/GET.yml")
    return jsonify(ret)

  @staticmethod
  @parse_params(
    Argument("user_id", location="args", type=str, required=True, help="The id of the user."),
    Argument("host", location="json", type=str, required=True, help="Host of the endpoint to be registered."),
    Argument("ssl", location="json", type=bool, required=True, help="Is SSL supported by the endpoint."),
    Argument("port", location="json", type=int, required=True, help="Port of the endpoint to be registered."),
    Argument("paths", location="json", type=list, required=True, help="Paths of the endpoint to be registered.")
  )
  @swag_from("../swagger/endpoint/POST.yml", validation=True)
  def post(user_id, host, port, ssl, paths):
    """ Create an endpoint entry """
    endpoint_urls = [ "http%(ssl)s://%(host)s%(port)s%(path)s" % \
                    {"ssl": "s" if ssl else "", "host": host, "port": '' if ssl else ":"+str(port), "path": path} \
                    for path in paths ]
    entries = []
    for endpoint_url in endpoint_urls:
      entry = EndpointRepository.create(user_token=user_id, destination_url=endpoint_url)
      entries.append(entry.json)
    ret = {"endpoints": entries}
    validate(ret, filepath="../swagger/endpoint/POST.yml")
    return jsonify(ret)

  @staticmethod
  @parse_params(
    Argument("id", location="args", type=str, required=True, help="The id of the endpoint."),
    Argument("host", location="json", type=str, required=True, help="Host of the endpoint to be updated."),
    Argument("ssl", location="json", type=bool, required=True, help="Is SSL supported by the endpoint."),
    Argument("port", location="json", type=int, required=True, help="Port of the endpoint to be updated."),
    Argument("path", location="json", type=str, required=True, help="Path of the endpoint to be updated.")
  )
  @swag_from("../swagger/endpoint/PUT.yml", validation=True)
  def put(id, host, port, ssl, path):
    """ Update an endpoint entry """
    destination_url = "http%(ssl)s://%(host)s%(port)s%(path)s" % \
                    {"ssl": "s" if ssl else "", "host": host, "port": '' if ssl else ":"+str(port), "path": path}

    entry = EndpointRepository().update(id=id, destination_url=destination_url)
    ret = {"endpoint": entry.json}
    validate(ret, filepath="../swagger/endpoint/PUT.yml")
    return jsonify(ret)

  @staticmethod
  @parse_params(
    Argument("id", location="values", required=True, type=str, help="The id of the endpoint.")
  )
  @swag_from("../swagger/endpoint/DELETE.yml", validation=False)
  def delete(id):
    """ Create an endpoint entry """
    prev = EndpointRepository.get(by='id', id=id)
    entry = EndpointRepository().delete(id=id)
    ret = {"endpoint": prev.json}
    validate(ret, filepath="../swagger/endpoint/DELETE.yml")
    return jsonify(ret)
