"""
Define the REST verbs for switch
"""

import json
from collections import namedtuple

from flasgger import swag_from, validate
from flask.json import jsonify
from flask_restful import Resource
from flask_restful.reqparse import Argument

from repositories import AccessRepository, EndpointRepository
from util import parse_params

import requests
from log import log


class Switch(Resource):
  @staticmethod
  @parse_params(
    Argument("user_id", location="args", type=str, required=True, help="The user token."),
    Argument("id", location="args", type=str, required=True, help="The endpoint id to be switched to."),
    Argument("verify_ssl_cert", type=bool, location="json", required=True, help="Should the SSL certificate be verified?"),
    Argument("headers", type=str, location="json", required=True, help="Request headers"),
    Argument("body", type=str, location="json", required=True, help="Request body"),
    Argument("arguments", type=str, location="json", required=True, help="Request arguments"),
    Argument("type", type=str, location="json", required=True, help="Request HTTP verb - GET, POST, PUT, DELETE"),
  )
  @swag_from("../swagger/switch/POST.yml", validation=True)
  def post(id, user_id, verify_ssl_cert, headers, body, arguments, type):
    """ Return an access log entry by id """
    endpoint = EndpointRepository.get(by='id', id=id)
    print(user_id, endpoint.json['user_token'])
    if user_id != endpoint.json['user_token']:
      return '{"error": "permission denied"}', 403

    url = endpoint.json['destination_url'] + (('?' + arguments) if arguments != '' else '')
    headers = json.loads(headers) if headers != '' else {}
    log.info("Hitting: %s" % url)

    # default response we send in case of any failure
    Failed = namedtuple('Failed', ['status_code', 'headers', 'text'])
    res = Failed(-1, {}, "{\"error\": \"Request type not valid.\"}")

    try:
      if type == 'GET':
        res = requests.get(url, headers=headers, verify=verify_ssl_cert)
      elif type == 'POST':
        res = requests.post(url, headers=headers, verify=verify_ssl_cert, json=body)
      elif type == 'PUT':
        res = requests.put(url, headers=headers, verify=verify_ssl_cert, json=body)
      elif type == 'DELETE':
        res = requests.delete(url, headers=headers, verify=verify_ssl_cert, json=body)
    except Exception as e:
      pass

    ret = {
      "status": res.status_code,
      "headers": dict(**res.headers),
      "body": res.text
    }

    # create an access entry __after__ the request is made
    access = AccessRepository.create(
      destination_url=url,
      user_id=user_id,
      data={
        "request": {
          "headers":   headers,
          "body":      body,
          "type":      type,
          "arguments": arguments
        },
        "response": ret
      }
    )
    log.info("User %s accessed URL %s" % (user_id, url))

    validate(ret, filepath="../swagger/switch/POST.yml")
    return jsonify(ret)

