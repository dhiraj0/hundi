"""
Define the Access metrics model
"""
from . import db
from .init import BaseModel, MetaBaseModel
from .audit import AuditMixin

from sqlalchemy.orm import relationship
from sqlalchemy.dialects.postgresql import JSONB


class AccessLog(db.Model, BaseModel, AuditMixin, metaclass=MetaBaseModel):
  """ The User model """

  __tablename__ = "access_log"

  user_id         = db.Column(db.String, nullable=False)
  destination_url = db.Column(db.String, nullable=False)
  data            = db.Column(JSONB())

  def __init__(self, user_id, destination_url, data=None):
    """ Create a new User """
    self.user_id         = user_id
    self.destination_url = destination_url
    self.data            = data
