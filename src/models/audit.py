"""
Define mixin for adding audit columns to tables
"""

from datetime import datetime
from sqlalchemy.orm import relationship
from sqlalchemy.ext.declarative import declared_attr

from . import db


class AuditMixin(object):
  created_at = db.Column(db.DateTime, default=datetime.now)
  updated_at = db.Column(db.DateTime, default=datetime.now, onupdate=datetime.now)
