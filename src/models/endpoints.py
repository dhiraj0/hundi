"""
Define the Access model
"""
from . import db
from .init import BaseModel, MetaBaseModel
from .audit import AuditMixin

from sqlalchemy.orm import relationship
from sqlalchemy.dialects.postgresql import JSONB


class Endpoint(db.Model, BaseModel, AuditMixin, metaclass=MetaBaseModel):
  """ The User model """

  __tablename__ = "endpoints"

  user_token      = db.Column(db.String, nullable=False)
  destination_url = db.Column(db.String, nullable=False)

  def __init__(self, user_token, destination_url):
    """ Create a new User """
    self.user_token      = user_token
    self.destination_url = destination_url
