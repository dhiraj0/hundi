#!/usr/bin/env python3

import os

from pathlib import Path
from dotenv import load_dotenv

from enum import Enum


TRUTH_VALUES = ['true', '1', 't', 'y', 'yes', 'yeah', 'yep', 'yup', 'certainly', 'uh-huh']

# Load .envrc
env_path = Path('.') / '.envrc'
load_dotenv(verbose=True, dotenv_path=env_path)

DEBUG = os.getenv("ENVIRONEMENT") == "DEV"
APPLICATION_ROOT = os.getenv("APPLICATION_APPLICATION_ROOT", "")
HOST = os.getenv("APPLICATION_HOST")
PORT = int(os.getenv("APPLICATION_PORT", "3000"))
SQLALCHEMY_TRACK_MODIFICATIONS = False

DB_CONTAINER = os.getenv("APPLICATION_DB_CONTAINER", "db")

POSTGRES = {
    "user": os.getenv("APPLICATION_POSTGRES_USER", "postgres"),
    "pw": os.getenv("APPLICATION_POSTGRES_PW", ""),
    "host": os.getenv("APPLICATION_POSTGRES_HOST", DB_CONTAINER),
    "port": int(os.getenv("APPLICATION_POSTGRES_PORT", "5432")),
    "db": os.getenv("APPLICATION_POSTGRES_DB", "postgres"),
}
DB_URI = "postgresql://%(user)s:%(pw)s@%(host)s:%(port)s/%(db)s" % POSTGRES

MIGRATE_ON_START = os.getenv("MIGRATE_ON_START", "False")
MIGRATE_ON_START = MIGRATE_ON_START.lower() in TRUTH_VALUES

LOG_IN_FILE = os.getenv("LOG_IN_FILE", "False")
LOG_IN_FILE = LOG_IN_FILE.lower() in TRUTH_VALUES

# ========= ONLY EQUITAS UAT CONFIGS BELOW =================================
# TODO: --- create a UAT conf, PROD conf separately-------------------------
EQUITAS_API_KEY = "dfqaj44bdqrwbe55npzy45mv"
EQUITAS_CS_KEY = "6cZq3Y0lV1Rq71GBwXhS8A=="  # for checksum
EQUITAS_CRYPT_KEY = "qVTyD/36Ts8R2qqtBYL7WQ=="  # for encryption

EQUITAS_EKYC_CONSTANTS = {
    "Mcc" : "6012",
    "PosEntryMode" : "019",
    "PosCode": "05",
    "cardAcceptorDetails": {
        "CA_ID": "EQT000000000001",
        "CA_TID": "11205764",
        "CA_TA": "CSB NERUL MUMBAI                    MHIN"
    },
}

EQUITAS_CONSTANTS = {
    "conversation_id_prefix": "SET",
    "bc_id": "821",
    "version": "1.0",
    "cde": "FI0029",
    "appId": "FOS",
    "brnchId": "9999",
    "usrId": "IBUSER",
    "usrId2": "344464",
    "homeBranch": "1001",
    "associatedWithCustomerLead": "1",
    "associatedWithAccountLead": "2",
    "customerAccountRelation": "SOW",
    "fieldEmployeeCode": "1",
    "FDProductCategoryCode": "PCAT04",
    "selfeFDRegularProductCode": "3118",
    "selfeFDSeniorCitizenProductCode": "3119",
    "accountOpeningValueDate": "2019-10-04",
    "authenticationToken": "e3XIdKW6zoWyJnBT0GeDghtUc7ChQXcMKP7TS+YltmgfXZJCEMLuKFgxM9RtZPcl"
}
PAYMENT_PLACEHOLDER_IDS = {
    "MICreditToESFBAccountNo": "222222222228",
    "MICreditToOtherBankAccountNo": "54267895578",
    "MICreditToOtherBankIFSC": "KKBK0008479",
    "MICreditToOtherBankName": "KOTAK MAHINDRA BANK LIMITED",
    "MICreditToOtherBankBranch": "ASHOK NAGAR BRANCH",
    "chequeIssuedbank": "Hdfc",
    "chequeNo": "3212",
    "fromESFBAccountNo": "100002084705",
    "iPayToESFBAccountNo": "222222222228",
    "iPayToOtherBankAccountNo": "54267895578",
    "iPayToOtherBankIFSC": "KKBK0008479",
    "iPayToOtherBankName": "KOTAK MAHINDRA BANK LIMITED",
    "iPayToOtherBankBranch": "ASHOK NAGAR BRANCH",



    "twelve_digit_upi_transaction_id": "789456123789" ,  # this is random shit I myself made up
}
pan = "AAIPM3854E"

occuapation = "50"
maritalStatus = "1"
first_name = "Dhiraj Bhakta"
last_name = "K"
title_id = "1"
gender = "M"
address = [
    {
        "country": "6",
        "pincode": "576101",
        "city": "62501",
        "addressType": "1",
        "mobileNumber": "919483895729",
        "district": "608",
        "addressLine1": "   SOME RANDOM PLACE",
        "addressLine2": " IN A RANDOM CITY",
        "addressLine3": " IN ONE OF THE 100 DISTRICTS",
        "state": "26",
        "landline": "0919483895729",
        "landmark": "Totally made up landmark"
    },
    {
        "country": "6",
        "pincode": "576101",
        "city": "62501",
        "addressType": "2",
        "mobileNumber": "919483895729",
        "district": "608",
        "addressLine1": "   SOME RANDOM PLACE",
        "addressLine2": " IN A RANDOM CITY",
        "addressLine3": " IN ONE OF THE 100 DISTRICTS",
        "state": "26",
        "landline": "0919483895729",
        "landmark": "Totally made up landmark"
    }
]

DOCUMENT_SSNAUTH = {
    "usrTkn": "newgen",
    "usrNm": "00474",
    "usrPwd": "00474"
}


class EQUITAS_ENDPOINTS(Enum):
    # the str values of the following endpoint enums need to be used while communicating with Equitas
    GET_OTP = "generateDigiOTP"
    VERIFY_OTP_AND_CREATE_CUST_LEAD = "createDigiCustLeadWithKYC"
    UPDATE_CUST_LEAD = "updateDigiCustLead"
    CREATE_CUST_BY_LEAD = "createDigiCustomerByLead"
    CREATE_ACCT_LEAD = "createDigiAccountByLead"
    UPDATE_ACCT_LEAD = "updateDigiAccountLead"
    FETCH_ACCT_LEAD = "fetchDigiAccountLead"
    CREATE_ACCT_BY_LEAD = "createDigiAccountByLead"
    GET_ACCT_SUMMARY = "getDigiAccountDetail"
    ADD_DOCUMENT = "addDocument"  # pending on me
    UPDATE_DOCUMENT = "updateDocument"  # pending on me
    GET_ACCT_MATURITY_DETAILS = "getDigiTDMaturityDetails"  # pending on me
    GET_ACCT_STATEMENT = "getDigiAccountStatement"  # pending on equitas
    REDEEM = "redeemDigiDeposit"  # pending on equitas


EQUITAS_BASE_URL = "https://apigwuat.equitasbank.com/uat/ext/digibnk"
EQUITAS_URLS = {
    EQUITAS_ENDPOINTS.GET_OTP : f'{EQUITAS_BASE_URL}/onboarding/generate/ekyc/otp/secure/v1',
    EQUITAS_ENDPOINTS.VERIFY_OTP_AND_CREATE_CUST_LEAD : f'{EQUITAS_BASE_URL}/onboarding/create/customer/otp/secure/v1',
    EQUITAS_ENDPOINTS.UPDATE_CUST_LEAD : f'{EQUITAS_BASE_URL}/onboarding/update/lead/secure/v1',
    EQUITAS_ENDPOINTS.CREATE_CUST_BY_LEAD : f'{EQUITAS_BASE_URL}/onboarding/create/customer/secure/v1',
    EQUITAS_ENDPOINTS.CREATE_ACCT_LEAD : f'{EQUITAS_BASE_URL}onboarding/create/acct/lead/secure/v1',
    EQUITAS_ENDPOINTS.FETCH_ACCT_LEAD : f'{EQUITAS_BASE_URL}/onboarding/fetch/accountlead/secure/v1',
    EQUITAS_ENDPOINTS.UPDATE_ACCT_LEAD: f'{EQUITAS_BASE_URL}/onboarding/update/accountlead/secure/v1',
    EQUITAS_ENDPOINTS.CREATE_ACCT_BY_LEAD : f'{EQUITAS_BASE_URL}/onboarding/create/account/secure/v1',
    EQUITAS_ENDPOINTS.ADD_DOCUMENT: f'{EQUITAS_BASE_URL}/onboarding/add/document/secure/v1',
    EQUITAS_ENDPOINTS.UPDATE_DOCUMENT : f'{EQUITAS_BASE_URL}/onboarding/update/docdetails/secure/v1',
    EQUITAS_ENDPOINTS.GET_ACCT_SUMMARY : f'{EQUITAS_BASE_URL}/acct/dtls/secure/v3',
    EQUITAS_ENDPOINTS.GET_ACCT_MATURITY_DETAILS : f'{EQUITAS_BASE_URL}/account/get/td/maturity/secure/v1',
    EQUITAS_ENDPOINTS.GET_ACCT_STATEMENT : f'{EQUITAS_BASE_URL}/account/statement/secure/v1',
    EQUITAS_ENDPOINTS.REDEEM: f'{EQUITAS_BASE_URL}/account/redeem/deposit/secure/v1',
}
