import base64
import json
import random
import xml.etree.ElementTree as ET
import requests

from Crypto.Cipher import AES
from Crypto.Hash import SHA512

from src import config
from src.util.xml_util import sanitizeXMLstring
from src.util.crypto_util import pad, unpad

from src.log import log


class EquitasClient:
    def __init__(self):
        self.headers = {
            "api_key": config.EQUITAS_API_KEY,
            "content-type": "application/json",
            "bc_id": config.EQUITAS_CONSTANTS["bc_id"]
        }
        self.encrypted_request = {
            "req_root": {
                "header": {
                    "cde": config.EQUITAS_CONSTANTS["cde"],
                    "requestId": "/** runtime requestId goes here*/",
                    "version": config.EQUITAS_CONSTANTS["version"],
                    "dateTime": "/** local timestamp in isoformat goes here */"
                },
                "body": {
                    "payload": "/**encrypted message goes here*/"
                }
            }
        }
        self.encrypted_response = None
        self.timestamp = None

    def setTimestamp(self, timestamp):
        self.timestamp = timestamp
        return self

    @staticmethod
    def makeXMLWithPayloadAndChecksum(messageString, checksumString):
        return sanitizeXMLstring(
            """
            <?xml version="1.0" encoding="UTF-8"?>
            <PIDBlock>
                <payload>{0}</payload>
                <checksum>{1}</checksum>
            </PIDBlock>
            """.format(messageString, checksumString))

    @staticmethod
    def parseXMLWithPayloadAndChecksum(xml_string):
        xml_root = ET.fromstring(xml_string)
        payload = xml_root.find("payload").text
        checksum = xml_root.find("checksum").text
        return payload, checksum

    @staticmethod
    def makeChecksum(messageString):
        toDigest = messageString + config.EQUITAS_CS_KEY
        return SHA512.new(toDigest.encode("utf-8")).hexdigest().upper()

    @staticmethod
    def validateChecksum(messageString, givenChecksumString):
        calculatedChecksumString = EquitasClient.makeChecksum(messageString)
        if(calculatedChecksumString != givenChecksumString):
            raise ValueError("checksum validation failed")

    @staticmethod
    def encrypt(messageString):
        key = base64.b64decode(config.EQUITAS_CRYPT_KEY.encode("utf-8"))
        cipher_aes = AES.new(key, AES.MODE_ECB)
        ciphertext = cipher_aes.encrypt(
            pad(
                messageString.encode("utf-8")
            )
        )
        return base64.b64encode(ciphertext).decode("utf-8")

    @staticmethod
    def decrypt(ciphertextString):
        ciphertext = base64.b64decode(ciphertextString.encode("utf-8"))
        key = base64.b64decode(config.EQUITAS_CRYPT_KEY.encode("utf-8"))
        cipher_aes = AES.new(key, AES.MODE_ECB)
        plaintext = unpad(
            cipher_aes.decrypt(ciphertext)
        )
        return plaintext.decode("utf-8")

    def request(self, url, messageObject, helptext="<insert purpose of request here/>"):
        messageString = json.dumps(messageObject)
        checksumString = EquitasClient.makeChecksum(messageString)

        xml = EquitasClient.makeXMLWithPayloadAndChecksum(messageString, checksumString)

        self.encrypted_request["req_root"]["header"]["requestId"] = str(random.randint(1, 5000))
        self.encrypted_request["req_root"]["header"]["dateTime"] = self.timestamp.isoformat()
        self.encrypted_request["req_root"]["body"]["payload"] = EquitasClient.encrypt(xml)

        # -----------------------------------------------------------------------------
        # |||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
        log.info("Sending %s request to EQUITAS: %s" % (helptext, url))
        log.info("headers: " + json.dumps(self.headers, indent=2))
        log.info("before encryption: " + json.dumps(messageObject, indent=2))
        log.info("request body :" + json.dumps(self.encrypted_request, indent=2))
        # |||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
        # -----------------------------------------------------------------------------
        response = requests.post(url, headers=self.headers,
                                 data=json.dumps(self.encrypted_request))
        response = json.loads(response.text)
        # -----------------------------------------------------------------------------
        # |||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
        log.info("Recieved response from EQUITAS(as-is):%s" % (json.dumps(response, indent=2)))
        # |||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
        # -----------------------------------------------------------------------------
        xml = EquitasClient.decrypt(response["req_root"]["body"]["payload"])

        messageString, checksumString = EquitasClient.parseXMLWithPayloadAndChecksum(xml)
        EquitasClient.validateChecksum(messageString, checksumString)
        messageObject = json.loads(messageString)
        # -----------------------------------------------------------------------------
        # |||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
        log.info("after decryption: " + json.dumps(messageObject, indent=2))
        # -----------------------------------------------------------------------------
        # |||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
        return messageObject
