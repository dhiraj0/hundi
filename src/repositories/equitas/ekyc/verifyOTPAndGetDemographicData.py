import random
import base64
import xml.dom.minidom as minidom
from src.util.xml_util import sanitizeXMLstring
from src.util.crypto_util import toBase64
import src.auth_pb2 as auth_pb2
from Crypto.Cipher import AES, PKCS1_OAEP, PKCS1_v1_5
from Crypto.PublicKey import RSA
from Crypto.Random import get_random_bytes
from Crypto.Hash import SHA512, SHA256
from Crypto import Random
import codecs
import xml.etree.ElementTree as ET

from datetime import datetime
import OpenSSL
from pathlib import Path


class VerifyOtpAndGetDemographicDataKUAPayloadManager:
    def __init__(self):
        self._request = None
        self._response = None
        self.timestamp = None
        self.aadhar_number = None
        self.otp = None
        self.stan = None
        self.card_acceptor_details = None
        self.dirpath = None
        self.PID = None

        self.uidai_public_key = None  # its same as uidai_cert lol
        self.uidai_cert_expiry_date = None
        self.session_key = None
        self.encrypted_session_key = None

        self.loadUIDAICert()
        self.buildSessionKey()

    def setRequest(self, request):
        self._request = request
        return self

    def setResponse(self, response):
        self._response = response
        return self

    def setTimestamp(self, ts):
        self.timestamp = ts
        return self

    def setAadhaar(self, aadhar_number):
        self.aadhar_number = aadhar_number
        return self

    def setOTP(self, otp):
        self.otp = otp
        return self

    def setSTAN(self, stan):
        self.stan = stan
        return self

    def setPathToDemographicdata(self, dirpath):
        self.dirpath = Path(dirpath)
        return self

    def setCardAcceptorDetails(self, card_acceptor_details):
        self.card_acceptor_details = card_acceptor_details
        return self

    def loadUIDAICert(self):
        cert = OpenSSL.crypto.load_certificate(OpenSSL.crypto.FILETYPE_PEM, open(
            'uidai_auth_encrypt_preprod.pem', 'rb').read())
        datetimestr = cert.get_notAfter().decode("utf-8")
        self.uidai_cert_expiry_date = datetime.strptime(datetimestr, "%Y%m%d%H%M%SZ")
        self.uidai_public_key = RSA.import_key(open("uidai_auth_encrypt_preprod.pem").read())

    def buildSessionKey(self):
        self.session_key = get_random_bytes(32)
        cipher_rsa = PKCS1_v1_5.new(self.uidai_public_key)
        self.encrypted_session_key = cipher_rsa.encrypt(self.session_key)

    def buildPIDBlock(self):
        # pid is in protobuf format
        ts = self.timestamp.strftime("%Y-%m-%dT%H:%M:%S")
        pid = auth_pb2.Pid()
        pid.ver = '2.0'
        pid.ts = ts
        pid.wadh = ''
        pid.pv.otp = self.otp
        self.PID = pid

    def encrypt(self, messageBytes):
        ts = self.timestamp.strftime("%Y-%m-%dT%H:%M:%S")
        iv = ts.encode("utf-8")[-12:]
        aad = ts.encode("utf-8")[-16:]
        cipher_aes = AES.new(self.session_key, AES.MODE_GCM, iv)
        cipher_aes.update(aad)
        cipher_text, tag = cipher_aes.encrypt_and_digest(messageBytes)
        return cipher_text, tag

    def buildRequestMessage(self):
        ts = self.timestamp.strftime("%Y-%m-%dT%H:%M:%S")
        self.buildPIDBlock()
        # encrypt PID with skey
        pid_cipher_text, pid_tag = self.encrypt(self.PID.SerializeToString())

        # encrypt PID hash with skey
        pid_hash = SHA256.new(self.PID.SerializeToString()).digest()
        pid_hash_cipher_text, pid_hash_tag = self.encrypt(pid_hash)

        self._request = sanitizeXMLstring(
            f"""<KycRequest>
                    <TransactionInfo>
                        <UID type ='U'>{self.aadhar_number}</UID>
                        <Transm_Date_time>{self.timestamp.utcnow().strftime("%m%d%H%M%S")}</Transm_Date_time>
                        <Stan>{self.stan}</Stan>
                        <Local_Trans_Time>{self.timestamp.strftime("%H%M%S")}</Local_Trans_Time>
                        <Local_date>{self.timestamp.strftime("%m%d")}</Local_date>
                        <CA_ID>{self.card_acceptor_details["CA_ID"]}</CA_ID>
                        <CA_TID>{self.card_acceptor_details["CA_TID"]}</CA_TID>
                        <CA_TA>{self.card_acceptor_details["CA_TA"]}</CA_TA>
                    </TransactionInfo>
                    <KycReqInfo  lr='N' pfr ='Y' ra ='F' rc ='Y' ver ='2.5'  de ='N'>
                        <Auth txn ='UKC:{self.stan}'>
                            <Uses pi='n' pa='n' pfa='n'  bio='n' otp='y'/>
                            <Meta/>
                            <Skey ci='{self.uidai_cert_expiry_date.strftime("%Y%m%d")}'>{toBase64(self.encrypted_session_key)}</Skey>
                            <Data type='P'>{toBase64(ts.encode("utf-8")+ pid_cipher_text+pid_tag)}</Data>
                            <Hmac>{toBase64(pid_hash_cipher_text+pid_hash_tag)}</Hmac>
                        </Auth>
                    </KycReqInfo>
            </KycRequest>"""
        )
        return self

    def unbuildErroneousResponseMessage(self):
        xml_root = ET.fromstring(self._response)
        kycRes = xml_root.find("resp").find("kycRes")

        kycRes = base64.b64decode(kycRes.text.encode("utf-8")).decode("utf-8")
        error_msg_xml_root = ET.fromstring(kycRes)
        Rar = error_msg_xml_root.find("Rar").text
        Rar = base64.b64decode(Rar.encode("utf-8")).decode("utf-8")
        innermost_error_msg_xml = minidom.parseString(Rar)
        return innermost_error_msg_xml.toprettyxml()

    def unbuildResponseMessage(self):
        xml_root = ET.fromstring(self._response)
        kycRes = xml_root.find("resp").find("kycRes")

        UidData = kycRes.find("UidData")
        # UiData will have -->LData Pht Poa Poi Prn

        # 1. write Aadhaar PDF to disk
        with open(self.dirpath / "my_aadhaar.pdf", "wb") as f:
            Prn = UidData.find("Prn")
            f.write(codecs.decode(Prn.text.encode("utf-8"), "base64"))
            UidData.remove(Prn)

        # 2. write Photo PNG to disk
        with open(self.dirpath / "my_photo.png", "wb") as f:
            Pht = UidData.find("Pht")
            f.write(codecs.decode(Pht.text.encode("utf-8"), "base64"))
            UidData.remove(Pht)

        # 3. write rest of demographic data in XML to disk
        with open(self.dirpath / "my_data.xml", "w") as f:
            rest_of_demographic_data_xml = ET.tostring(UidData, encoding="unicode")
            xml = minidom.parseString(rest_of_demographic_data_xml)
            f.write(xml.toprettyxml())
