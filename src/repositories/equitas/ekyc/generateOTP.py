import random
import xml.dom.minidom as minidom
from src.util.xml_util import sanitizeXMLstring
import xml.etree.ElementTree as ET


class GenerateOtpKUAPayloadManager:
    def __init__(self):
        self._request = None
        self._response = None
        self.timestamp = None
        self.aadhaar_number = None
        self.merchant_category_code = None
        self.pos_entry_mode = None
        self.pos_code = None
        self.card_acceptor_details = None

        self.stan = None

    def setRequest(self, request):
        self._request = request
        return self

    def setResponse(self, response):
        self._response = response
        return self

    def setTimestamp(self, ts):
        self.timestamp = ts
        return self

    def setAadhaar(self, aadhar):
        self.aadhaar_number = aadhar
        return self

    def setMerchantCategoryCode(self, mcc):
        self.merchant_category_code = mcc
        return self

    def setPosEntryMode(self, pos_entry_mode):
        self.pos_entry_mode = pos_entry_mode
        return self

    def setPosCode(self, pos_code):
        self.pos_code = pos_code
        return self

    def setCardAcceptorDetails(self, card_acceptor_details):
        self.card_acceptor_details = card_acceptor_details
        return self

    def buildRequestMessage(self):
        self.stan = str(random.randint(99999, 999999))  # gotta be 6 digits
        self._request = sanitizeXMLstring(
            f"""<OtpRequest>
            <TransactionInfo>
                    <UID type='U'>{self.aadhaar_number}</UID>
                    <Transm_Date_time>{self.timestamp.utcnow().strftime("%m%d%H%M%S")}</Transm_Date_time>
                    <Stan>{self.stan}</Stan>
                    <Local_Trans_Time>{self.timestamp.strftime("%H%M%S")}</Local_Trans_Time>
                    <Local_date>{self.timestamp.strftime("%m%d")}</Local_date>
                    <Mcc>{self.merchant_category_code}</Mcc>
                    <Pos_entry_mode>{self.pos_entry_mode}</Pos_entry_mode>
                    <Pos_code>{self.pos_code}</Pos_code>
                    <CA_ID>{self.card_acceptor_details["CA_ID"]}</CA_ID>
                    <CA_TID>{self.card_acceptor_details["CA_TID"]}</CA_TID>
                    <CA_TA>{self.card_acceptor_details["CA_TA"]}</CA_TA>
            </TransactionInfo>
            <Opts ch='01'/>
        </OtpRequest>"""
        )
        return self

    def unbuildResponseMessage(self):
        # for now it just pretty formats response xml
        xml = minidom.parseString(self._response)
        self._response = xml.toprettyxml()
        return self._response
