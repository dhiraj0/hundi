from src.repositories.equitas.ekyc.generateOTP import GenerateOtpKUAPayloadManager
from src.repositories.equitas.ekyc.verifyOTPAndGetDemographicData import VerifyOtpAndGetDemographicDataKUAPayloadManager
from src.repositories.equitas.payloadManagers.ekycGetOtpPayloadManager import EKYCGetOTPPayloadManager
from src.repositories.equitas.payloadManagers.createCustLeadPayloadManager import CreateCustLeadPayloadManager
from src.repositories.equitas.payloadManagers.updateCustLeadPayloadManager import UpdateCustLeadPayloadManager
from src.repositories.equitas.payloadManagers.createCustPayloadManager import CreateCustPayloadManager
from src.repositories.equitas.payloadManagers.createAcctLeadPayloadManager import CreateAcctLeadPayloadManager
from src.repositories.equitas.payloadManagers.fetchAcctLeadPayloadManager import FetchAcctLeadPayloadManager
from src.repositories.equitas.payloadManagers.updateAcctLeadPayloadManager import UpdateAcctLeadPayloadManager
from src.repositories.equitas.payloadManagers.createAcctPayloadManager import CreateAcctPayloadManager

from src.repositories.equitas.payloadManagers.addDocPayloadManager import AddDocPayloadManager
from src.repositories.equitas.payloadManagers.updateDocPayloadManager import UpdateDocPayloadManager

from src.repositories.equitas.payloadManagers.fetchAcctSummaryPayloadManager import FetchAcctSummaryPayloadManager
from src.repositories.equitas.payloadManagers.fetchAcctMaturityDetailsPayloadManager import FetchAcctMaturityDetailsPayloadManager
from src.repositories.equitas.payloadManagers.fetchAcctStmtPayloadManager import FetchAcctStmtPayloadManager
from src.repositories.equitas.equitasClient import EquitasClient

from src.log import log
from datetime import datetime
from src.config import EQUITAS_EKYC_CONSTANTS, EQUITAS_URLS, EQUITAS_CONSTANTS
import json

print(f"""
    1. Get OTP                             |
    2. Verify OTP & Create customer lead   |
    3. Update customer lead                |
    4. Create customer                     
    =======================================
    5. Create Account Lead
    6. Fetch Account Lead details
    7. Update Account Lead
    8. Create Account
    =======================================|
    9a. Add document (AOF)                 |
    9b. Add document (Aadhaar)             |
    10. Update document (AOF)              |
    =======================================|
    11. Fetch Account Summary              |
    12. Fetch Account Maturity Details
    13. Fetch Account Statement
    =======================================
    14. Redeem ( premature withdrawal )
    """)

choice = input(">>> ").lower().rstrip()
if choice == "1":
    aadhaar = "562711904686"

    now = datetime.now()
    kuaPM = GenerateOtpKUAPayloadManager()\
        .setTimestamp(now)\
        .setAadhaar(aadhaar)\
        .setPosCode(EQUITAS_EKYC_CONSTANTS["PosCode"])\
        .setPosEntryMode(EQUITAS_EKYC_CONSTANTS["PosEntryMode"])\
        .setMerchantCategoryCode(EQUITAS_EKYC_CONSTANTS["Mcc"])\
        .setCardAcceptorDetails(EQUITAS_EKYC_CONSTANTS["cardAcceptorDetails"])\
        .buildRequestMessage()

    apiPM = EKYCGetOTPPayloadManager()\
        .setTimestamp(now)\
        .setEkycPayload(kuaPM._request)\
        .buildRequestMessage()

    client = EquitasClient()\
        .setTimestamp(now)
    url = EQUITAS_URLS[EKYCGetOTPPayloadManager.endpoint]

    responseObject = client.request(url, apiPM._request, "Get Aadhaar OTP")

    msgBdy = apiPM.setResponse(responseObject).unbuildResponseMessage()
    # log.info(msgBdy)
    kuaResponse = kuaPM.setResponse(msgBdy).unbuildResponseMessage()

    log.info("Kua(sarvatra) response: " + kuaResponse)
    log.warning("OTP has been sent to your mobile; \nNOTE DOWN : => stan = " + kuaPM.stan)


elif choice == "2":
    print("\n Hope you have noted the STAN from earlier response and noted the OTP sent to your mobile?")
    print("Enter OTP :")
    otp = input(">>> ")
    print("Enter STAN :")
    stan = input(">>>")

    aadhaar = "562711904686"
    mobile_number = "919483895729"
    mother_name = "surekha"

    now = datetime.now()
    kuaPM = VerifyOtpAndGetDemographicDataKUAPayloadManager()\
        .setTimestamp(now)\
        .setAadhaar(aadhaar)\
        .setSTAN(stan)\
        .setOTP(otp)\
        .setPathToDemographicdata("/home/dhirajbhakta/Documents")\
        .setCardAcceptorDetails(EQUITAS_EKYC_CONSTANTS["cardAcceptorDetails"])\
        .buildRequestMessage()

    log.info("KUA embedded request:" + kuaPM._request)

    apiPM = CreateCustLeadPayloadManager()\
        .setTimestamp(now)\
        .setMobileNumber(mobile_number)\
        .setMothername(mother_name)\
        .setEkycPayload(kuaPM._request)\
        .buildRequestMessage()

    client = EquitasClient()\
        .setTimestamp(now)
    url = EQUITAS_URLS[CreateCustLeadPayloadManager.endpoint]

    responseObject = client.request(
        url, apiPM._request, "Verify Aadhaar OTP & create customer lead")

    msgBdy = apiPM.setResponse(responseObject).unbuildResponseMessage()
    # log.error(msgBdy)
    kuaPM.setResponse(msgBdy).unbuildResponseMessage()

    log.warning("Demographic details fetched and placed in :" + kuaPM.dirpath.as_posix())
    log.warning(
        "Customer Lead has been successfully created: \nNOTE DOWN : => customerLeadId = " + apiPM.cust_lead_id)

elif choice == "3":
    print("Hope you've noted the Customer Lead ID from earlier response")
    print("Enter Customer Lead ID:")
    cust_lead_id = input(">>> ")

    first_name = "Dhiraj Bhakta"
    last_name = "K"
    title_id = "1"
    email = "dhirajbhakta110@gmail.com"
    dob = "19951115"
    gender = "M"
    father_name = "Father"
    mother_name = "Mother"
    mobile_number = "919483895729"
    address = {
        "country_id": "6",
        "pincode": "576101",
        "city_id": "62501",
        "mobile_number": mobile_number,
        "district": "608",
        "address_line_1": "   SOME RANDOM PLACE",
        "address_line_2": " IN A RANDOM CITY",
        "address_line_3": " IN ONE OF THE 100 DISTRICTS",
        "state_id": "26",
        "landline": "0919483895729",
        "landmark": "Totally made up landmark"
    }

    gross_annual_income_id = "4"
    occupation_id = "50"
    qualification_id = "5"
    position_id = "6"

    marital_status_id = "1"

    city_of_birth = "Mangalore"
    country_of_birth = "INDIA"

    PAN = "AGXPM3070D"

    now = datetime.now()
    apiPM = UpdateCustLeadPayloadManager()\
        .setTimestamp(now)\
        .setCustomerLeadId(cust_lead_id)\
        .setTitleId(title_id)\
        .setFirstName(first_name)\
        .setLastName(last_name)\
        .setGender(gender)\
        .setDateOfBirth(dob)\
        .setEmail(email)\
        .setMobileNumber(mobile_number)\
        .setAddressDetails(address)\
        .setFatherName(father_name)\
        .setMotherName(mother_name)\
        .setCityOfBirth(city_of_birth)\
        .setCountryOfBirth(country_of_birth)\
        .setMaritalStatusId(marital_status_id)\
        .setGrossIncomeCategoryId(gross_annual_income_id)\
        .setOccupationId(occupation_id)\
        .setQualificationId(qualification_id)\
        .setPositionId(position_id)\
        .setPAN(PAN)\
        .setPhotoFilePath("/home/dhirajbhakta/Documents/my_photo.png")\
        .buildRequestMessage()

    client = EquitasClient().setTimestamp(now)
    url = EQUITAS_URLS[UpdateCustLeadPayloadManager.endpoint]
    responseObject = client.request(
        url, apiPM._request, "Update customer lead details")

    msgBdy = apiPM.setResponse(responseObject).unbuildResponseMessage()
    log.info("Customer lead successfully updated")


elif choice == "4":
    print("Hope you've noted the Customer Lead ID from earlier response")
    print("Enter Customer Lead ID:")
    cust_lead_id = input(">>> ")

    now = datetime.now()
    apiPM = CreateCustPayloadManager()\
        .setTimestamp(now)\
        .setCustomerLeadId(cust_lead_id)\
        .buildRequestMessage()

    client = EquitasClient().setTimestamp(now)
    url = EQUITAS_URLS[CreateCustPayloadManager.endpoint]

    responseObject = client.request(
        url, apiPM._request, "Create Customer")
    msgBdy = apiPM.setResponse(responseObject).unbuildResponseMessage()
    log.info("Customer successfully created")

elif choice == "5":
    print("Hope you've noted the Customer Lead ID, Customer ID from earlier response")
    print("Enter Customer Lead ID:")
    cust_lead_id = input(">>> ")
    print("Enter Customer ID:")
    cust_id = input(">>> ")

    customer_name = "DHIRAJ BHAKTA"
    account_title = "DHIRAJ BHAKTA a/c"
    initial_deposit_amount = "800"

    now = datetime.now()
    apiPM = CreateAcctLeadPayloadManager()\
        .setTimestamp(now)\
        .setCustomerLeadId(cust_lead_id)\
        .setCustomerId(cust_id)\
        .setCustomerName(customer_name)\
        .setAccountTitle(account_title)\
        .setInitialDepositAmount(initial_deposit_amount)\
        .buildRequestMessage()

    client = EquitasClient().setTimestamp(now)
    url = EQUITAS_URLS[CreateAcctLeadPayloadManager.endpoint]

    responseObject = client.request(
        url, apiPM._request, "Create Account Lead")
    msgBdy = apiPM.setResponse(responseObject).unbuildResponseMessage()
    log.info("Account Lead  successfully created")

elif choice == "6":
    print("Hope you've noted the Account Lead ID from earlier response")
    print("Enter Account Lead ID:")
    acct_lead_id = input(">>> ")

    now = datetime.now()
    apiPM = FetchAcctLeadPayloadManager()\
        .setTimestamp(now)\
        .setAccountLeadId(acct_lead_id)\
        .buildRequestMessage()

    client = EquitasClient().setTimestamp(now)
    url = EQUITAS_URLS[FetchAcctLeadPayloadManager.endpoint]

    responseObject = client.request(
        url, apiPM._request, "Fetch Account Lead details")
    msgBdy = apiPM.setResponse(responseObject).unbuildResponseMessage()
    log.info("Account Lead  successfully created")

elif choice == "7":
    print("Hope you've noted the Customer Lead ID, Customer ID, preference ID, deliverable ID from earlier response")
    print("Enter Customer Lead ID:")
    cust_lead_id = input(">>> ")
    print("Enter Customer ID:")
    cust_id = input(">>> ")
    print("Enter Preference ID :")
    preference_id = input(">>> ")
    print("Enter Deliverable ID :")
    deliverable_id = input(">>> ")

    customer_name = "DHIRAJ BHAKTA"
    account_title = "DHIRAJ BHAKTA a/c"
    initial_deposit_amount = "800"
    tenure_in_days = "300"

    now = datetime.now()
    apiPM = UpdateAcctLeadPayloadManager()\
        .setTimestamp(now)\
        .setCustomerLeadId(cust_lead_id)\
        .setCustomerId(cust_id)\
        .setCustomerName(customer_name)\
        .setAccountTitle(account_title)\
        .setInitialDepositAmount(initial_deposit_amount)\
        .setPreferenceId(preference_id)\
        .setDeliverableId(deliverable_id)\
        .setTenureInDays(tenure_in_days)\
        .setCustomerAccountLeadRelationId()\
        .buildRequestMessage()

    client = EquitasClient().setTimestamp(now)
    url = EQUITAS_URLS[UpdateAcctLeadPayloadManager.endpoint]

    responseObject = client.request(
        url, apiPM._request, "Update Account Lead")
    msgBdy = apiPM.setResponse(responseObject).unbuildResponseMessage()
    log.info("Account Lead  successfully updated")


elif choice == "8":
    print("Hope you've noted the Account Lead ID from earlier response")
    print("Enter Account Lead ID:")
    acct_lead_id = input(">>> ")

    now = datetime.now()
    apiPM = CreateAcctPayloadManager()\
        .setTimestamp(now)\
        .setAccountLeadId(acct_lead_id)\
        .buildRequestMessage()

    client = EquitasClient().setTimestamp(now)
    url = EQUITAS_URLS[CreateAcctPayloadManager.endpoint]

    responseObject = client.request(
        url, apiPM._request, "Fetch Account Lead details")
    msgBdy = apiPM.setResponse(responseObject).unbuildResponseMessage()
    log.info("Customer successfully created")


elif choice == "9a":
    print("Hope you've noted the Customer Lead ID from earlier response")
    print("Enter Customer Lead ID:")
    cust_lead_id = input(">>> ")

    document_name = "ACCOUNT_OPENING_FORM_9483895729.pdf"
    document_type = "LEAD"
    document_type_id = 276
    document_category = "ACCOPENREQ"
    document_category_id = 201
    document_sub_category = "ACCOUNT_OPENING_FORM"
    document_sub_category_id = 204
    comments = "Addition of document for account creation"

    branchId = 34464

    now = datetime.now()
    apiPM = AddDocPayloadManager()\
        .setTimestamp(now)\
        .setDocFilepath("/home/dhirajbhakta/Documents/AOF.pdf")\
        .setDocumentName(document_name)\
        .setDocType(document_type)\
        .setDocTypeId(document_type_id)\
        .setDocCategory(document_category)\
        .setDocCategoryId(document_category_id)\
        .setDocSubCategory(document_sub_category)\
        .setDocSubCategoryId(document_sub_category_id)\
        .setBranchId(branchId)\
        .setLeadId(cust_lead_id)\
        .buildRequestMessage()

    client = EquitasClient().setTimestamp(now)
    url = EQUITAS_URLS[AddDocPayloadManager.endpoint]

    responseObject = client.request(
        url, apiPM._request, "Upload Account Opening Form")
    msgBdy = apiPM.setResponse(responseObject).unbuildResponseMessage()
    log.warn(msgBdy)
    log.info("AOF pdf successfully uploaded")

elif choice == "9b":
    print("Hope you've noted the Account Lead ID from earlier response")
    print("Enter Account Lead ID:")
    account_lead_id = input(">>> ")

    document_name = "UIDAI_ADHAAR_RESPONSE_9483895729.pdf"
    document_type = "LEAD"
    document_type_id = 384
    document_category = "UIDAIData"
    document_category_id = 70
    document_sub_category = "UIDAI_ADHAAR_RESPONSE"
    document_sub_category_id = 12
    comments = "Addition of document for account creation"

    branchId = 34464

    now = datetime.now()
    apiPM = AddDocPayloadManager()\
        .setTimestamp(now)\
        .setDocFilepath("/home/dhirajbhakta/Documents/my_aadhaar.pdf")\
        .setDocumentName(document_name)\
        .setDocType(document_type)\
        .setDocTypeId(document_type_id)\
        .setDocCategory(document_category)\
        .setDocCategoryId(document_category_id)\
        .setDocSubCategory(document_sub_category)\
        .setDocSubCategoryId(document_sub_category_id)\
        .setBranchId(branchId)\
        .setLeadId(account_lead_id)\
        .buildRequestMessage()

    client = EquitasClient().setTimestamp(now)
    url = EQUITAS_URLS[AddDocPayloadManager.endpoint]

    responseObject = client.request(
        url, apiPM._request, "Upload Aadhaar PDF")
    msgBdy = apiPM.setResponse(responseObject).unbuildResponseMessage()
    log.info("aadhaar pdf successfully uploaded")


elif choice == "10":
    print("Hope you've noted the Account Lead Id, Account ID from earlier response")
    print("Enter Account  Lead ID:")
    acct_lead_id = input(">>> ")
    print("Enter Account  ID:")
    acct_id = input(">>> ")
    print("Enter DMS document ID :")
    dms_doc_id = input(">>> ")

    document_name = "ACCOUNT_OPENING_FORM_9483895729.pdf"
    document_type_id = 276
    document_category_id = 201
    document_sub_category_id = 204
    document_category_id = 201
    associated_with = EQUITAS_CONSTANTS["associatedWithAccountLead"]

    now = datetime.now()
    apiPM = UpdateDocPayloadManager()\
        .setTimestamp(now)\
        .setAccountId(acct_id)\
        .setAccountLeadId(acct_lead_id)\
        .setDocumentName(document_name)\
        .setDocumentTypeId(document_type_id)\
        .setCategoryId(document_category_id)\
        .setSubCategoryId(document_sub_category_id)\
        .setDMSDocId(dms_doc_id)\
        .setAssociatedWith(associated_with)\
        .buildRequestMessage()

    client = EquitasClient().setTimestamp(now)
    url = EQUITAS_URLS[UpdateDocPayloadManager.endpoint]

    responseObject = client.request(
        url, apiPM._request, "Update document AOF pdf")
    msgBdy = apiPM.setResponse(responseObject).unbuildResponseMessage()
    log.info("AOF document successfully updated")

elif choice == "11":
    print("Hope you've noted the Account ID from earlier response")
    print("Enter Account ID:")
    acct_id = input(">>> ")

    now = datetime.now()
    apiPM = FetchAcctSummaryPayloadManager()\
        .setTimestamp(now)\
        .setAccountId(acct_id)\
        .buildRequestMessage()

    client = EquitasClient().setTimestamp(now)
    url = EQUITAS_URLS[FetchAcctSummaryPayloadManager.endpoint]

    responseObject = client.request(
        url, apiPM._request, "Fetch Account summary ")
    msgBdy = apiPM.setResponse(responseObject).unbuildResponseMessage()
    log.info("Successfully fetched Account summary")

elif choice == "12":
    print("Hope you've noted the Account ID from earlier response")
    print("Enter Account ID:")
    acct_id = input(">>> ")

    now = datetime.now()
    apiPM = FetchAcctMaturityDetailsPayloadManager()\
        .setTimestamp(now)\
        .setAccountId(acct_id)\
        .buildRequestMessage()

    client = EquitasClient().setTimestamp(now)
    url = EQUITAS_URLS[FetchAcctMaturityDetailsPayloadManager.endpoint]

    responseObject = client.request(
        url, apiPM._request, "Fetch Account Maturity details")
    msgBdy = apiPM.setResponse(responseObject).unbuildResponseMessage()
    log.info("Successfully fetched Account maturity details")

elif choice == "13":
    print("Hope you've noted the Account ID from earlier response")
    print("Enter Account ID:")
    acct_id = input(">>> ")
    print("Enter From date (YYYYMMDD) :")
    from_date = input(">>> ")
    from_date = datetime.strptime(from_date, "%Y%m%d")
    print("Enter To date (YYYYMMDD) :")
    to_date = input(">>> ")
    to_date = datetime.strptime(to_date, "%Y%m%d")

    now = datetime.now()
    apiPM = FetchAcctStmtPayloadManager()\
        .setTimestamp(now)\
        .setAccountId(acct_id)\
        .setFromDate(from_date)\
        .setToDate(to_date)\
        .buildRequestMessage()

    client = EquitasClient().setTimestamp(now)
    url = EQUITAS_URLS[FetchAcctStmtPayloadManager.endpoint]

    responseObject = client.request(
        url, apiPM._request, "Fetch Account statement ")
    msgBdy = apiPM.setResponse(responseObject).unbuildResponseMessage()
    log.info("Successfully fetched Account statement ")


else:
    print("Invalid choice, please choose again\n")
