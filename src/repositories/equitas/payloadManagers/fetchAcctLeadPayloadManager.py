import src.config as config
from datetime import datetime
from dateutil.relativedelta import *
from src.util.file_util import readFileInB64
from .payloadManager import PayloadManager


class FetchAcctLeadPayloadManager(PayloadManager):
    endpoint = config.EQUITAS_ENDPOINTS.FETCH_ACCT_LEAD

    def __init__(self):
        self.setRequestLabel("fetchDigiAccountLeadReq")
        self.setResponseLabel("fetchDigiAccountLeadRep")
        self.account_lead_id = None

    def setAccountLeadId(self, account_lead_id):
        self.account_lead_id = account_lead_id

    def buildRequestMessage(self):
        super().buildRequestMessage()
        self._request[self._request_label]["msgBdy"] = {
            "accountLeadID": self.account_lead_id,
            "authenticationToken": config.EQUITAS_CONSTANTS["authenticationToken"]
        }
        return self
