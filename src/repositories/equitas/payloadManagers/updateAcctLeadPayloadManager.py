import src.config as config
from datetime import datetime
from dateutil.relativedelta import *
from src.util.file_util import readFileInB64
from .payloadManager import PayloadManager


class UpdateAcctLeadPayloadManager(PayloadManager):
    endpoint = config.EQUITAS_ENDPOINTS.UPDATE_ACCT_LEAD

    def __init__(self):
        self.setRequestLabel("updateDigiCustLeadReq")
        self.setResponseLabel("updateDigiCustLeadRep")
        self.cust_id = None
        self.account_lead_id = None
        self.customer_full_name = None
        self.account_title = None
        self.initial_deposit_amount = None
        self.tenure_days = None
        self.preference_id = None  # get this from fetchAcctLead response
        self.deliverable_id = None  # get this from fetchAcctLead response
        self.customer_account_lead_relation_id = None  # get this from fetchAcctLead response

    def setCustomerId(self, cust_id):
        self.cust_id = cust_id
        return self

    def setAccountLeadId(self, account_lead_id):
        self.account_lead_id = account_lead_id
        return self

    def setCustomerName(self, full_name):
        self.customer_full_name = full_name
        return self

    def setAccountTitle(self, account_title):
        self.account_title = account_title
        return self

    def setInitialDepositAmount(self, initial_deposit_amount):
        self.initial_deposit_amount = initial_deposit_amount
        return self

    def setPreferenceId(self, preference_id):
        self.preference_id = preference_id
        return self

    def setDeliverableId(self, deliverable_id):
        self.deliverable_id = deliverable_id
        return self

    def setCustomerAccountLeadRelationId(self, customer_account_lead_relation_id):
        self.customer_account_lead_relation_id = customer_account_lead_relation_id
        return self

    def setTenureInDays(self, tenure_days):
        self.tenure_days = tenure_days
        return self

    def buildRequestMessage(self):
        super().buildRequestMessage()
        self._request[self._request_label]["msgBdy"] = {
            "customerPreferences": [
                {
                    "netBanking": True,  # what is this?
                    "mobileBankingNumber": True,  # what is this?
                    "preferenceID": self.preference_id,
                    "netBankingRights": "3",  # wtf is this?, why 3?
                    "sms": True,
                    "mappedCustomer": self.cust_id,
                    "passbook": False,
                    "allSMSAlerts": True,
                    "onlyTransactionAlerts": True,
                    "mappedAccountLead": self.account_lead_id,
                    "physicalStatement": False,
                    "emailStatement": True
                }
            ],
            "nomineeAddress": [],  # user can choose to not provide any nominees
            "nominee": {},  # user can choose to not provide any nominees
            "fromMADP": True,  # what is MADP?
            "usrId": 1,  # why is this always 1??
            "accountLeadID": self.account_lead_id,
            "isQC": False,  # what is QC?
            "customerDeliverables": [
                {
                    "welcomeKit": True,  # what is welcome kit?
                    "debitCard": False,
                    "iKITChequeBook": False,
                    "mappedCustomer": self.cust_id,
                    "chequeBook": False,
                    "iKIT": False,  # what is iKIT?
                    "deliverableID": self.deliverable_id
                }
            ],
            "customerAccountLeadRelation": [
                {
                    "isPrimaryHolder": True,  # why to pass this again ??
                    # why to pass this again???
                    "customerAccountRelation": config.EQUITAS_CONSTANTS["customerAccountRelation"],
                    "id": self.customer_account_lead_relation_id,
                    "UCIC": self.cust_id,  # why again??
                    "completedDataEntryD0": "0",  # why again???
                    "customerName": self.customer_full_name.upper()  # why again????
                }
            ],
            "accountLead": {
                # ??
                "MICreditToESFBAccountNo": config.PAYMENT_PLACEHOLDER_IDS["MICreditToESFBAccountNo"],
                "isPriorityLead": False,  # ??
                "leadAssignedTo": "1",  # ??
                "cashTransactionRefNo": config.PAYMENT_PLACEHOLDER_IDS["tweleve_digit_upi_transaction_id"],
                "sourcingChannel": "6",  # ??
                "initialDepositType": "5",  # ??
                "accountTitle": self.account_title,  # why provide this again??
                "fieldEmployeeCode": "1",  # ??
                "transactionNarration": config.PAYMENT_PLACEHOLDER_IDS["twelve_digit_upi_transaction_id"] + " | SETU | Truecaller",
                "accountOpeningValueDate": config.EQUITAS_CONSTANTS["accountOpeningValueDate"],
                "currencyOfDeposit": "INR",
                "productCategory": config.EQUITAS_CONSTANTS["FDProductCategoryCode"],
                "sourceOfFund": "1",  # ??
                "purposeOfOpeningAccount": "1",  # ??
                "interestPayout": "0",  # ??
                "interestCompoundFrequency": "0",  # ??
                "modeOfOperation": "Single",
                "tenureDays": self.tenure_days,
                "MICreditToOtherBankAccountNo": config.PAYMENT_PLACEHOLDER_IDS["MICreditToOtherBankAccountNo"],
                # remitter bank IFSC code
                "chequeIssuedBank": config.PAYMENT_PLACEHOLDER_IDS["chequeIssuedBank"],
                "accountOpeningBranch": config.EQUITAS_CONSTANTS["homeBranch"],
                "iPayToOtherBankName": config.PAYMENT_PLACEHOLDER_IDS["iPayToOtherBankName"],
                "depositAmount": self.initial_deposit_amount,  # how is this different from initial_deposit_amount?
                "iPayToOtherBankIFSC": config.PAYMENT_PLACEHOLDER_IDS["iPayToOtherBankIFSC"],
                "tenureMonths": "0",  # why should tenureMonths be 0?
                "sourceBranch": config.EQUITAS_CONSTANTS["homeBranch"],
                "chequeNo": config.PAYMENT_PLACEHOLDER_IDS["chequeNo"],
                "isDelete": False,
                "maturityInstruction": "1",  # ?
                "accountType": "1",  # why again??
                "accountOpeningFlow": "1",  # why again??
                "MICreditToOtherBankBranch": config.PAYMENT_PLACEHOLDER_IDS["MICreditToOtherBankBranch"],
                "iPayToOtherBankBranch": config.PAYMENT_PLACEHOLDER_IDS["iPayToOtherBankBranch"],
                "iPayToOtherBankBenificiaryName": "Dhiraj",
                # how is this different from depositAmount?? what that fuck dude seriously??
                "initialDepositAmount": self.initial_deposit_amount,
                "MICreditToOtherBankIFSC": config.PAYMENT_PLACEHOLDER_IDS["MICreditToOtherBankIFSC"],
                "initialDepositMode": "5",  # why again??
                "iPayToESFBAccountNo": config.PAYMENT_PLACEHOLDER_IDS["iPayToESFBAccountNo"],
                # why again,.pls stop this nonsense
                "productVariant": config.EQUITAS_CONSTANTS["selfeFDRegularProductCode"],
                "sourceofLead": "11",  # why again?
                "fromESFBAccountNo": config.PAYMENT_PLACEHOLDER_IDS["fromESFBAccountNo"],
                "iPayToOtherBankAccountNo": config.PAYMENT_PLACEHOLDER_IDS["iPayToOtherBankAccountNo"],
                "otherSourceOfLead": "SETU",  # why again?
                "MICreditToOtherBankName": config.PAYMENT_PLACEHOLDER_IDS["MICreditToOtherBankName"]
            },
            "guardianAddress": [],  # will i ever have to enter this?
            "guardian": {},  # will i ever have to enter this?
            "authenticationToken": config.EQUITAS_CONSTANTS["authenticationToken"],
        }
        return self
