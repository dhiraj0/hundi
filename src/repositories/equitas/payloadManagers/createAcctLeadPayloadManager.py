import src.config as config
from datetime import datetime
from dateutil.relativedelta import *
from src.util.file_util import readFileInB64
from .payloadManager import PayloadManager


class CreateAcctLeadPayloadManager(PayloadManager):
    endpoint = config.EQUITAS_ENDPOINTS.CREATE_ACCT_LEAD

    def __init__(self):
        self.setRequestLabel("createDigiAccountLeadReq")
        self.setResponseLabel("createDigiAccountLeadRep")
        self.cust_lead_id = None
        self.cust_id = None
        self.customer_full_name = None
        self.account_title = None
        self.initial_deposit_amount = None

    def setCustomerLeadId(self, cust_lead_id):
        self.cust_lead_id = cust_lead_id
        return self

    def setCustomerId(self, cust_id):
        self.cust_id = cust_id
        return self

    def setCustomerName(self, fullname):
        self.customer_full_name = fullname.upper()
        return self

    def setAccountTitle(self, title):
        self.account_title = title
        return self

    def setInitialDepositAmount(self, amt):
        self.initial_deposit_amount = amt
        return self

    def buildRequestMessage(self):
        super().buildRequestMessage()
        self._request[self._request_label]["msgBdy"] = {
            "customerAccountLeadRelation": [{
                "isPrimaryHolder": True,  # What is this?
                "isDelete": False,  # what is this?
                "customerAccountRelation": config.EQUITAS_CONSTANTS["customerAccountRelation"],
                "id": "",  # why empty?
                "completedDataEntryD0": "1",  # what does this mean?
                # why to pass this again? we've already given it in "customer creation"
                "customerName": self.customer_full_name,
                "isInsert": True,  # what does True do? what does false do?
                "customerLeadID": self.cust_lead_id,
                "UCIC": self.cust_id
            }],
            "accountLead": {
                # why should sourceBranch be same as home branch? what the heck is home branch anyway?
                "sourceBranch": config.EQUITAS_CONSTANTS["homeBranch"],
                "isPriorityLead": False,  # why false lol?
                "sourcingChannel": "10",  # why 10?
                "initialDepositType": "5",  # why 5?
                "accountTitle": "DHIRAJ BHAKTA ACC",  # what is the significance of this?h
                "isDelete": False,  # what is this?
                "fieldEmployeeCode": config.EQUITAS_ENDPOINTS["fieldEmployeeCode"],
                "accountType": "1",  # 1 stands for Single Account; 3 stands for Joint account
                # 1 stands for Non-Insta Kit flow ( what is this though?)
                "accountOpeningFlow": "1",
                "currencyOfDeposit": "INR",
                # PCAT01:Savings PCAT02:Current PCAT04:FD PCAT05:RD
                "productCategory": config.EQUITAS_CONSTANTS["FDProductCategoryCode"],
                "sourceOfFund": "1",  # what is this?
                "initialDepositAmount": self.initial_deposit_amount,
                "initialDepositMode": "5",  # what is this?
                "productCode": config.EQUITAS_CONSTANTS["selfeFDRegularProductCode"],
                "modeOfOperation": "Single",  # what is "mode of operation"? why "single"?
                "isRestrictedAccount": False,  # what is a "restricted account"?
                "sourceofLead": "11",
                "otherSourceOfLead": "Setu",
                "sourceOfLeadCreation": "5",
                "fatcaDeclaration": True,  # why True?
                # why accountOpeningBranch should be same as homeBranch?
                "accountOpeningBranch": config.EQUITAS_CONSTANTS["homeBranch"]
            },
            "fromMADP": True,  # what is MADP? why True?
            "usrId": "1",  # what is 1?
            "accountLeadID": "",  # why leave empty?
            "authenticationToken": config.EQUITAS_CONSTANTS["authenticationToken"]
        }
        return self
