import base64
import src.config as config
from datetime import datetime
from dateutil.relativedelta import *
from src.util.file_util import readFileInB64
from .payloadManager import PayloadManager


class UpdateCustLeadPayloadManager(PayloadManager):
    endpoint = config.EQUITAS_ENDPOINTS.UPDATE_CUST_LEAD

    def __init__(self):
        self.setRequestLabel("updateDigiCustLeadReq")
        self.setResponseLabel("updateDigiCustLeadRep")

        self.cust_lead_id = None

        self.address = None
        self.title_id = None  # 1:Mister 2:Miss etc
        self.first_name = None
        self.last_name = None  # check mail for first name last name rules
        self.father_name = None  # check mail for first name last name rules
        self.mother_name = None  # mother's maiden name
        self.gender = None
        self.date_of_birth = None
        self.age = None
        self.mobile_number = None
        self.email_id = None
        self.pan = None
        self.occupation_id = None
        self.gross_annual_income_category_id = None
        self.marital_status_id = None
        self.prospect_qualification_id = None
        self.prospect_position_id = None
        self.city_of_birth = None
        self.country_of_birth = None
        self.photo_b64 = None

    def setCustomerLeadId(self, cust_lead_id):
        self.cust_lead_id = cust_lead_id
        return self

    def setAddressDetails(self, address_details):
        self.address = address_details
        return self

    def setTitleId(self, title_id):
        self.title_id = title_id
        return self

    def setFirstName(self, first_name):
        self.first_name = first_name
        return self

    def setLastName(self, last_name):
        self.last_name = last_name
        return self

    def setFatherName(self, father_name):
        self.father_name = father_name
        return self

    def setMotherName(self, mother_name):
        self.mother_name = mother_name
        return self

    def setGender(self, gender):
        self.gender = gender
        return self

    def setDateOfBirth(self, YYYYmmdd):
        self.date_of_birth = datetime.strptime(YYYYmmdd, "%Y%m%d")
        self.age = relativedelta(datetime.now(), self.date_of_birth).years
        return self

    def setMobileNumber(self, mobile_number):
        self.mobile_number = mobile_number
        return self

    def setEmail(self, email_id):
        self.email_id = email_id
        return self

    def setPAN(self, pan):
        self.pan = pan
        return self

    def setOccupationId(self, occupation_id):
        self.occupation_id = occupation_id
        return self

    def setGrossIncomeCategoryId(self, gross_annual_income_category_id):
        self.gross_annual_income_category_id = gross_annual_income_category_id
        return self

    def setMaritalStatusId(self, marital_status_id):
        self.marital_status_id = marital_status_id
        return self

    def setQualificationId(self, prospect_qualification_id):
        self.prospect_qualification_id = prospect_qualification_id
        return self

    def setPositionId(self, prospect_position_id):
        self.prospect_position_id = prospect_position_id
        return self

    def setCityOfBirth(self, city_of_birth):
        self.city_of_birth = city_of_birth
        return self

    def setCountryOfBirth(self, country_of_birth):
        self.country_of_birth = country_of_birth
        return self

    def setPhotoFilePath(self, photo_file_path):
        self.photo_b64 = readFileInB64(photo_file_path)
        return self

    def buildRequestMessage(self):
        super().buildRequestMessage()
        self._request[self._request_label]["msgBdy"] = {
            "prospectProfileDetails": {
                "qualification": self.prospect_qualification_id,
                "position": self.prospect_position_id
            },
            "associatedWith": config.EQUITAS_CONSTANTS["associatedWithCustomerLead"],
            "isCurrentAddressSameAsPermanent": True,
            "entityIdentityDetails": [],
            "addressDetails": [
                {
                    "country": self.address["country_id"],
                    "pincode": self.address["pincode"],
                    "city": self.address["city_id"],
                    "addressType": "1",
                    "mobileNumber": self.address["mobile_number"],
                    "district": self.address["district"],
                    # any restrictions on length of address lines?
                    "addressLine1": self.address["address_line_1"],
                    "addressLine2": self.address["address_line_2"],
                    "addressLine3": self.address["address_line_3"],
                    "state": self.address["state_id"],
                    "landline": self.address["landline"],
                    "landmark": self.address["landmark"],
                },
                # compulsory to have both current and permanent address
                {
                    "country": self.address["country_id"],
                    "pincode": self.address["pincode"],
                    "city": self.address["city_id"],
                    "addressType": "2",
                    "mobileNumber": self.address["mobile_number"],
                    "district": self.address["district"],
                    # any restrictions on length of address lines?
                    "addressLine1": self.address["address_line_1"],
                    "addressLine2": self.address["address_line_2"],
                    "addressLine3": self.address["address_line_3"],
                    "state": self.address["state_id"],
                    # landline compulsory? it is weird as hell lol
                    "landline": self.address["landline"],
                    "landmark": self.address["landmark"],
                },
            ],
            "DocumentsDetails": [],
            "aboutLeadDetails": {
                "homeBranch": config.EQUITAS_CONSTANTS["homeBranch"]
            },
            "aboutProspectDetails": {
                "lastName": self.last_name,
                "fatherName": self.father_name,
                "preferredLanguage": "ENG",
                "gender": self.gender,
                "motherMaidenName": self.mother_name,
                "mobileNumber": self.mobile_number,
                "grossAnnualIncome": self.gross_annual_income_category_id,
                "emailID": self.email_id,
                "title": self.title_id,
                "firstName": self.first_name,
                "nationality": "IND",
                "dob": self.date_of_birth.strftime("%Y-%m-%d"),
                "occuapation": self.occupation_id,
                "middleName": "",
                "age": self.age,
                "maritalStatus": self.marital_status_id
            },
            "ID": self.cust_lead_id,
            "IdentityInfoDetails": {
                "form": "16",  # 16 stands for PAN card
                        "photo": self.photo_b64,
                        "panNumber": self.pan
            },
            "FatcaTaxDetailsIndividual": {
                "cityOfBirth": self.city_of_birth,
                "countryOfBirth": self.country_of_birth.upper()
            },
            "authenticationToken": config.EQUITAS_CONSTANTS["authenticationToken"]
        }
        return self
