import random
import src.config as config


class PayloadManager:

    def __init__(self):
        self.timestamp = None
        self._request = None
        self._response = None
        self._request_label = None
        self._response_label = None
        self._response_msg = None

    def setTimestamp(self, ts):
        self.timestamp = ts
        self.conversation_id = config.EQUITAS_CONSTANTS["conversation_id_prefix"]\
            + ts.strftime("%Y%m%d%H%M%S%f") + str(random.randint(10, 99))
        return self

    def setRequestLabel(self, label):
        self._request_label = label

    def setResponseLabel(self, label):
        self._response_label = label

    def setRequest(self, request):
        self._request = request

    def setResponse(self, response):
        self._response = response
        return self

    def buildRequestMessage(self):
        self._request = {
            self._request_label: {
                "msgHdr": {
                    "cnvId": self.conversation_id,
                    "bizObjId": self.conversation_id,
                    "appId": config.EQUITAS_CONSTANTS["appId"],
                    "msgId": self.conversation_id,
                    "extRefId": self.conversation_id,
                    "timestamp": self.timestamp.utcnow().isoformat(),
                    "authInfo": {
                        "usrId": config.EQUITAS_CONSTANTS["usrId"],
                        "brnchId": config.EQUITAS_CONSTANTS["brnchId"]
                    }
                },
                "msgBdy": {}
            }
        }
        return self._request

    def unbuildResponseMessage(self):
        isOk = self._response[self._response_label]["msgHdr"]["rslt"]
        msgBdy = self._response[self._response_label]["msgBdy"]
        if isOk != "OK":
            raise ValueError("response message is NOT OK")
        return msgBdy
