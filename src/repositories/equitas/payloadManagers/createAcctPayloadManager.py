import src.config as config
from datetime import datetime
from dateutil.relativedelta import *
from src.util.file_util import readFileInB64
from .payloadManager import PayloadManager


class CreateAcctPayloadManager(PayloadManager):
    endpoint = config.EQUITAS_ENDPOINTS.CREATE_ACCT_BY_LEAD

    def __init__(self):
        self.setRequestLabel("createDigiAccountByLeadReq")
        self.setResponseLabel("createDigiAccountByLeadRep")
        self.account_lead_id = None

    def setAccountLeadId(self, account_lead_id):
        self.account_lead_id = account_lead_id

    def buildRequestMessage(self):
        super().buildRequestMessage()
        self._request[self._request_label]["msgBdy"] = {
            "associatedWith": config.EQUITAS_CONSTANTS["associatedWithAccountLead"],
            "accountLeadID": self.account_lead_id,
            "authenticationToken": config.EQUITAS_CONSTANTS["authenticationToken"]
        }
