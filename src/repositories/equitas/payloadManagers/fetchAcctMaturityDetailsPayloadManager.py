import src.config as config
from datetime import datetime
from dateutil.relativedelta import *
from src.util.file_util import readFileInB64
from .payloadManager import PayloadManager


class FetchAcctMaturityDetailsPayloadManager(PayloadManager):
    endpoint = config.EQUITAS_ENDPOINTS.GET_ACCT_MATURITY_DETAILS

    def __init__(self):
        self.setRequestLabel("getDigiTDMaturityDetailsRequest")
        self.setResponseLabel("getDigiTDMaturityDetailsResponse")
        self.account_id = None

    def setAccountId(self, account_id):
        self.account_id = account_id
        return self

    def buildRequestMessage(self):
        super().buildRequestMessage()
        self._request[self._request_label]["msgBdy"] = {
            "tdAcctNb": self.account_id,
            "dpstNb": "1"  # what is this?
        }
        return self
