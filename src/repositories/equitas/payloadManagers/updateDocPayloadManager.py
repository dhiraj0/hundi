import src.config as config
from datetime import datetime
from dateutil.relativedelta import *
from src.util.file_util import readFileInB64
from .payloadManager import PayloadManager


class UpdateDocPayloadManager(PayloadManager):
    endpoint = config.EQUITAS_ENDPOINTS.UPDATE_DOCUMENT

    def __init__(self):
        self.setRequestLabel("updateDigiDocumentDetailsReq")
        self.setResponseLabel("updateDigiDocumentDetailsRep")
        self.account_lead_id = None
        self.account_id = None
        self.category_id = None
        self.sub_category_id = None
        self.associated_with = None
        self.doc_type_id = None
        self.dms_doc_id = None
        self.doc_name = None

    def setAccountLeadId(self, account_lead_id):
        self.account_lead_id = account_lead_id
        return self

    def setAccountId(self, account_id):
        self.account_id = account_id
        return self

    def setDocumentName(self, doc_name):
        self.doc_name = doc_name
        return self

    def setDocumentTypeId(self, doc_type_id):
        self.doc_type_id = doc_type_id
        return self

    def setDMSDocId(self, dms_doc_id):
        self.dms_doc_id = dms_doc_id
        return self

    def setCategoryId(self, category_id):
        self.category_id = category_id
        return self

    def setSubCategoryId(self, sub_category_id):
        self.sub_category_id = sub_category_id
        return self

    def setAssociatedWith(self, associated_with):
        self.associated_with = associated_with
        return self

    def buildRequestMessage(self):
        super().buildRequestMessage()
        self._request[self._request_label]["msgBdy"] = {
            "documents": [
                {
                    "documentNo": self.doc_name,
                    "associatedWith": self.associated_with,
                    "documentType": self.doc_type_id,
                    "mappedAccountLead": self.account_lead_id,
                    "mappedAccount": self.account_id,
                    "categoryCode": self.category_id,
                    "dmsDocumentId": self.dms_doc_id,  # get this from addDoc API response
                    "subcategoryCode": self.sub_category_id
                }
            ],
            "authToken": config.EQUITAS_CONSTANTS["authenticationToken"]
        }
        return self
