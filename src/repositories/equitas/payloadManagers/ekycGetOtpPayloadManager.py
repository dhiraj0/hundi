import base64
import src.config as config
from datetime import datetime
from dateutil.relativedelta import *
from src.util.file_util import readFileInB64
from src.util.crypto_util import toBase64
from .payloadManager import PayloadManager


class EKYCGetOTPPayloadManager(PayloadManager):
    endpoint = config.EQUITAS_ENDPOINTS.GET_OTP

    def __init__(self):
        self.setRequestLabel("generateDigiOTPRequest")
        self.setResponseLabel("generateDigiOTPResponse")
        self.ekycPayload = None

    def setEkycPayload(self, payload):
        self.ekycPayload = payload
        return self

    def buildRequestMessage(self):
        super().buildRequestMessage()
        self._request[self._request_label]["msgBdy"] = {
            "eKYCXMLStringReqPayload": toBase64(self.ekycPayload)
        }
        return self

    def unbuildResponseMessage(self):
        msgBdy = super().unbuildResponseMessage()
        return base64.b64decode(msgBdy["eKYCXMLStringRepPayload"].encode("utf-8")).decode("utf-8")
