import src.config as config
from datetime import datetime
from dateutil.relativedelta import *
from src.util.file_util import readFileInB64
from .payloadManager import PayloadManager


class FetchAcctStmtPayloadManager(PayloadManager):
    endpoint = config.EQUITAS_ENDPOINTS.GET_ACCT_STATEMENT

    def __init__(self):
        self.setRequestLabel("getDigiAccountStatementRequest")
        self.setResponseLabel("getDigiAccountStatementResponse")
        self.account_id = None
        self.from_date = None
        self.to_date = None

    def setAccountId(self, account_id):
        self.account_id = account_id
        return self

    def setFromDate(self, from_date):
        self.from_date = from_date
        return self

    def setToDate(self, to_date):
        self.to_date = to_date
        return self

    def buildRequestMessage(self):
        super().buildRequestMessage()
        # check out pagination options for this API
        self._request[self._request_label]["msgBdy"] = {
            "acctId": self.account_id,
            "frDt": self.from_date.strftime("%Y-%m-%d"),
            "toDt": self.to_date.strftime("%Y-%m-%d")
        }
        return self
