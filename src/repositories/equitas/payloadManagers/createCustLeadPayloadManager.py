import base64
import src.config as config
from datetime import datetime
from dateutil.relativedelta import *
from src.util.file_util import readFileInB64
from .payloadManager import PayloadManager
from src.util.crypto_util import toBase64


class CreateCustLeadPayloadManager(PayloadManager):
    endpoint = config.EQUITAS_ENDPOINTS.VERIFY_OTP_AND_CREATE_CUST_LEAD

    def __init__(self):
        self.setRequestLabel("createDigiCustLeadwitheKYCReq")
        self.setResponseLabel("createDigiCustLeadwitheKYCRep")
        self.ekycPayload = None
        self.mother_name = None
        self.mobile_number = None
        self.pan = None

        self.cust_lead_id = None  # populate from response

    def setEkycPayload(self, payload):
        self.ekycPayload = payload
        return self

    def setMothername(self, mother_name):
        self.mother_name = mother_name
        return self

    def setMobileNumber(self, mobile_number):
        self.mobile_number = mobile_number
        return self

    def setPan(self, pan):
        self.pan = pan
        return self

    def buildRequestMessage(self):
        super().buildRequestMessage()
        self._request[self._request_label]["msgBdy"] = {
            "eKYCDetails": {
                "eKYCXMLStringReqPayload": toBase64(self.ekycPayload)
            },
            "leadDetails": {
                "individualEntry": {
                    "motherMaidenName": self.mother_name,
                    "mobilePhone": self.mobile_number,
                    "identityType": "16",
                    "PAN": self.pan
                },
                "ignoreProbableMatch": "true",
                "entityType": "I",
                "fromMADP": "false",  # ?
                "deDupChkReqByCustCount": "Y",  # ?
                "isAadhar": "true",  # ?
                "mappedToAccountLead": "false",
                "usrId": config.EQUITAS_CONSTANTS["usrId2"],
                "authenticationToken": config.EQUITAS_CONSTANTS["authenticationToken"],
                "entityFlagType": "I"  # ??
            }
        }
        return self

    def unbuildResponseMessage(self):
        msgBdy = super().unbuildResponseMessage()
        self.cust_lead_id = msgBdy["leadDetails"]["customerLeadID"]
        return base64.b64decode(msgBdy["KycDetails"]["eKYCXMLStringRepPayload"].encode("utf-8")).decode("utf-8")
