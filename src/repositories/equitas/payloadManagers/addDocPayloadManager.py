import src.config as config
from datetime import datetime
from dateutil.relativedelta import *
from src.util.file_util import readFileInB64
from .payloadManager import PayloadManager


class AddDocPayloadManager(PayloadManager):
    endpoint = config.EQUITAS_ENDPOINTS.ADD_DOCUMENT

    def __init__(self):
        self.setRequestLabel("addDocumentReq")
        self.setResponseLabel("addDocumentRep")
        # can be customer_lead_id or account_lead_id based on context(what kind of document is being uploaded)
        self.lead_id = None
        self.branch_id = None
        self.doc_b64 = None
        self.doc_type = None
        self.doc_type_id = None
        self.doc_category = None
        self.doc_category_id = None
        self.doc_sub_category = None
        self.doc_sub_category_id = None
        self.doc_name = None
        self.comments = None

    def setLeadId(self, lead_id):
        self.lead_id = lead_id
        return self

    def setBranchId(self, branch_id):
        self.branch_id = branch_id
        return self

    def setDocumentName(self, doc_name):
        self.doc_name = doc_name
        return self

    def setDocType(self, doc_type):
        self.doc_type = doc_type
        return self

    def setDocTypeId(self, doc_type_id):
        self.doc_type_id = doc_type_id
        return self

    def setDocCategory(self, doc_category):
        self.doc_category = doc_category
        return self

    def setDocCategoryId(self, doc_category_id):
        self.doc_category_id = doc_category_id
        return self

    def setDocSubCategory(self, doc_sub_category):
        self.doc_sub_category = doc_sub_category
        return self

    def setDocSubCategoryId(self, doc_sub_category_id):
        self.doc_sub_category_id = doc_sub_category_id
        return self

    def setComments(self, comments):
        self.comments = comments
        return self

    def setDocFilepath(self, filepath):
        self.doc_b64 = readFileInB64(filepath)
        return self

    def buildRequestMessage(self):
        super().buildRequestMessage()
        self._request[self._request_label]["msgBdy"] = {
            "ssnAuth": {  # whats all this for?
                "usrTkn": config.DOCUMENT_SSNAUTH["usrTkn"],
                "usrNm": config.DOCUMENT_SSNAUTH["usrNm"],
                "usrPwd": config.DOCUMENT_SSNAUTH["usrPwd"]
            },
            "addDocRq": [
                {
                    "docTp": self.doc_type,
                    "docSbCtgry": self.doc_sub_category,
                    "docNm": self.doc_name,
                    "docCtgryCd": self.doc_category_id,
                    "docCatg": self.doc_category,
                    "docTypCd": self.doc_type_id,
                    "docSbCtgryCd": self.doc_sub_category_id,
                    "flLoc": "",  # what is this and why is this always empty?
                    "docCmnts": self.comments,
                    "bsPyld": self.doc_b64,

                    "docRefId": [
                        {
                            "idTp": "LEDID",
                            "id": self.lead_id
                        },
                        {
                            "idTp": "BRNCH",
                            "id": self.branch_id
                        }
                    ]
                }
            ]
        }
        return self
