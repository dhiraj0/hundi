import src.config as config
from datetime import datetime
from dateutil.relativedelta import *
from src.util.file_util import readFileInB64
from .payloadManager import PayloadManager


class CreateCustPayloadManager(PayloadManager):
    endpoint = config.EQUITAS_ENDPOINTS.CREATE_CUST_BY_LEAD

    def __init__(self):
        self.setRequestLabel("createDigiCustomerByLeadReq")
        self.setResponseLabel("createDigiCustomerByLeadRep")
        self.cust_lead_id = None

    def setCustomerLeadId(self, cust_lead_id):
        self.cust_lead_id = cust_lead_id
        return self

    def buildRequestMessage(self):
        super().buildRequestMessage()
        self._request[self._request_label]["msgBdy"] = {
            "associatedWith": config.EQUITAS_CONSTANTS["associatedWithCustomerLead"],
            "custLeadID": self.cust_lead_id,
            "authenticationToken": config.EQUITAS_CONSTANTS["authenticationToken"]
        }
        return self
