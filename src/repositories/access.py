""" Defines the User repository """

from models import AccessLog


class AccessRepository:
  """ The repository for the access log model """

  @staticmethod
  def get(by, id=None, user_id=None):
    """ Query a user by last and first name """
    if by == 'id' and id is not None:
      return AccessLog.query.filter_by(id=id).one()
    elif by == 'user_id' and user_id is not None:
      return AccessLog.query.filter_by(user_id=user_id).all()
    else:
      raise Exception('No entries supplied for selecting user by %s' % by)

  @staticmethod
  def create(user_id, destination_url, data=None):
    """ Create a new user """
    alog = AccessLog(user_id=user_id, destination_url=destination_url, data=data)
    return alog.save()
