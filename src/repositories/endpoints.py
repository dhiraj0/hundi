""" Defines the User repository """

from models import Endpoint


class EndpointRepository:
  """ The repository for the resource log model """

  @staticmethod
  def get(by, id=None, user_token=None):
    """ Query a user by last and first name """
    if by == 'id' and id is not None:
      return Endpoint.query.filter_by(id=id).one()
    elif by == 'user_token' and user_token is not None:
      return Endpoint.query.filter_by(user_token=user_token).all()
    else:
      raise Exception('No entries supplied for selecting resource by %s' % by)

  @staticmethod
  def create(user_token, destination_url):
    """ Create a new user """
    res = Endpoint(user_token=user_token, destination_url=destination_url)
    return res.save()

  def update(self, id, user_token=None, destination_url=None):
    """ Update a user's details """
    res = self.get(by='id', id=id)
    if user_token is not None:
      res.user_token = user_token
    if destination_url is not None:
      res.destination_url = destination_url

    return res.save()

  def delete(self, id):
    res = self.get(by='id', id=id)
    return res.delete()
