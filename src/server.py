#!/usr/bin/env python3

from flasgger import Swagger
from flask import Flask
from flask.blueprints import Blueprint
from flask_log_request_id import RequestID, RequestIDLogFilter
from flask_migrate import  Migrate, upgrade

from log import log
import config
import routes
from models import db

# config your API specs
# you can define multiple specs in the case your api has multiple versions
# omit configs to get the default (all views exposed in /spec url)
# rule_filter is a callable that receives "Rule" object and
#   returns a boolean to filter in only desired views

server = Flask(__name__)
RequestID(server)

server.config["SWAGGER"] = {
  "swagger_version": "2.0",
  "title":           "Setu isengard",
  "specs":           [
    {
      "version":     "0.1.0",
      "title":       "Setu isengard",
      "endpoint":    "spec",
      "route":       "/spec",
      "rule_filter": lambda rule: True,  # all in
    }
  ],
  "static_url_path": "/docs",
  "specs_route":     "/docs/"
}

Swagger(server)

log.warning("███████╗███████╗████████╗██╗   ██╗")
log.warning("██╔════╝██╔════╝╚══██╔══╝██║   ██║")
log.warning("███████╗█████╗     ██║   ██║   ██║")
log.warning("╚════██║██╔══╝     ██║   ██║   ██║")
log.warning("███████║███████╗   ██║   ╚██████╔╝")
log.warning("╚══════╝╚══════╝   ╚═╝    ╚═════╝ ")


server.debug                                          = config.DEBUG
server.config["SQLALCHEMY_DATABASE_URI"]              = config.DB_URI
server.config["SQLALCHEMY_TRACK_MODIFICATIONS"]       = config.SQLALCHEMY_TRACK_MODIFICATIONS
server.config["LOG_REQUEST_ID_GENERATE_IF_NOT_FOUND"] = True

db.init_app(server)
db.app = server

if config.MIGRATE_ON_START:
  log.warning("Running DB upgrade")
  with server.app_context():
    migrate = Migrate(server, db)
    upgrade('migrations')

for blueprint in vars(routes).values():
  if isinstance(blueprint, Blueprint):
    server.register_blueprint(blueprint, url_prefix=config.APPLICATION_ROOT)

if __name__ == "__main__":
  server.run(host=config.HOST, port=config.PORT)
