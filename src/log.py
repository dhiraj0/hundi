#!/usr/bin/env python3

import logging
import os

import coloredlogs
from flask_log_request_id import RequestIDLogFilter

import config

FILE_LOG_FORMAT = "%(levelname)s: %(asctime)s %(filename)s:%(lineno)d request_id=%(request_id)s \
pid:%(process)s module:%(module)s %(message)s"

if config.LOG_IN_FILE:
  handler = logging.FileHandler('server.log')
  handler.setFormatter(logging.Formatter(FILE_LOG_FORMAT))
  handler.addFilter(RequestIDLogFilter())
  handler.setLevel(logging.DEBUG)
  logging.getLogger().addHandler(handler)

CONSOLE_LOG_FORMAT = "%(levelname)s: %(asctime)s %(filename)s:%(lineno)d → %(message)s"

CONSOLE_LEVEL_STYLES = {
  "critical": {"color": "red", "bright": True, "bold": True},
  "debug":    {"color": "green", "faint": True},
  "error":    {"color": "red", "bright": True},
  "info":     {"color": "green", "bold": True},
  "notice":   {"color": "cyan"},
  "spam":     {"color": "white", "faint": True},
  "success":  {"color": "green", "bold": True},
  "verbose":  {"color": "blue"},
  "warning":  {"color": "magenta", "bold": True},
}

# console logging format
console = logging.StreamHandler()
console.setLevel(logging.DEBUG)
# set a format which is simpler for console use
formatter = logging.Formatter(CONSOLE_LOG_FORMAT)
# tell the handler to use this format
console.setFormatter(formatter)
# add the handler to the root logger
logging.getLogger().addHandler(console)

coloredlogs.install(
  level="DEBUG", fmt=CONSOLE_LOG_FORMAT, level_styles=CONSOLE_LEVEL_STYLES
)

log = logging.getLogger(__name__)
