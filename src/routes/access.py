"""
Defines the blueprint for the users
"""
from flask import Blueprint
from flask_restful import Api

from resources import GetAccessLog, GetAccessLogByUser

ACCESS_BLUEPRINT = Blueprint("access", __name__)
Api(ACCESS_BLUEPRINT).add_resource(
  GetAccessLog, "/log"
)

Api(ACCESS_BLUEPRINT).add_resource(
  GetAccessLogByUser, "/log/user"
)
