from .access import ACCESS_BLUEPRINT
from .endpoints import ENDPOINT_BLUEPRINT
from .switch import SWITCH_BLUEPRINT
