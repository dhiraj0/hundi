"""
Defines the blueprint for the users
"""
from flask import Blueprint
from flask_restful import Api

from resources import Switch

SWITCH_BLUEPRINT = Blueprint("switch", __name__)
Api(SWITCH_BLUEPRINT).add_resource(
  Switch, "/call"
)

