"""
Defines the blueprint for the users
"""
from flask import Blueprint
from flask_restful import Api

from resources import EndpointManager

ENDPOINT_BLUEPRINT = Blueprint("endpoints", __name__)
Api(ENDPOINT_BLUEPRINT).add_resource(
  EndpointManager, "/endpoints"
)

